import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Events} from '@ionic/angular';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public formGroup: FormGroup;
    private emailPattern = '^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z]{2,4})$)';

    constructor(private fb: FormBuilder,
                private router: Router,
                private events: Events,
                private authenticationService: AuthenticationService) {
        this.formGroup = this.fb.group({
            email: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(40),
                Validators.required, Validators.pattern(this.emailPattern)])],
            password: ['', Validators.required],
        }, {updateOn: 'submit'});
    }

    ngOnInit() {
    }

    login() {
        if (this.formGroup.valid) {
            const request = {
                email: this.formGroup.value.email,
                password: this.formGroup.value.password,
                appType: 'web',
                appName: 'KuvikNotes'
            };
            this.authenticationService.login(request).subscribe(response => {
                this.events.publish('note:login');
                this.router.navigateByUrl('/home/dashboard');
            }, error => {
                // this.toastMessageProvider.presentToast('Email or Password is not valid');
            });
        } else {

        }
    }

    register() {
        this.router.navigateByUrl('/user/registration');
    }

    forgetPassword() {
        this.router.navigateByUrl('/user/forget-password');
    }

    reset() {
        console.log('Resetting the form');
        this.formGroup.reset();
    }
}

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LoginComponent} from './login.component';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Events, IonicModule, NavController} from '@ionic/angular';
import {MomentModule} from 'angular2-moment';
import {CKEditorModule} from 'ng2-ckeditor';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {LinkyModule} from 'angular-linky';

describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule,
                IonicModule,
                RouterTestingModule,
                HttpClientModule,
                CKEditorModule,
                MomentModule,
                LinkyModule,
                ReactiveFormsModule],
            providers: [
                FormBuilder, Events, HttpClient, NavController
            ],
            declarations: [LoginComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

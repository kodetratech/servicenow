import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Events} from '@ionic/angular';
import {ToastMessageService} from '../../services/toast-message.service';
import {AuthenticationService} from '../../services/authentication.service';
import {ValidationService} from '../../services/validation.service';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
    public formGroup: FormGroup;
    public formSubmitAttempt = false;

    constructor(private fb: FormBuilder,
                private router: Router,
                private events: Events,
                private validationService: ValidationService,
                private toastMessageService: ToastMessageService,
                private authenticationService: AuthenticationService) {
        this.formGroup = this.fb.group({
            firstName: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 25,
                null, null))],
            lastName: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 25, null, null))],
            email: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 25, null, null))],
            password: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 25, null, null))],
            phone: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 25, null, null))],
            role: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 100, null, null))],
            street1: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 100, null, null))],
            street2: ['', Validators.compose(validationService.getValidatorsFunctionArray(false, 2, 50, null, null))],
            city: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 100, null, null))],
            state: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 2, 50, null, null))],
            postalCode: ['', Validators.compose(validationService.getValidatorsFunctionArray(true, 5, 10, null, null))],
        }, {updateOn: 'submit'});
    }

    ngOnInit() {
    }

    reset() {
        console.log('Resetting the form');
        this.formGroup.reset();
        this.formSubmitAttempt = false;
    }

    register() {
        console.log('Form values ', this.formGroup.value);
        console.log('Form ', this.formGroup);
        this.formSubmitAttempt = true;
        if (this.formGroup.valid) {
            const userInfo = {
                firstName: this.formGroup.value.firstName,
                lastName: this.formGroup.value.lastName,
                email: this.formGroup.value.email,
                password: this.formGroup.value.password,
                phone: this.formGroup.value.phone,
                street1: this.formGroup.value.street1,
                street2: this.formGroup.value.street2,
                city: this.formGroup.value.city,
                state: this.formGroup.value.state,
                postalCode: this.formGroup.value.postalCode,
                role: this.formGroup.value.role,
                appType: 'ionic-web',
                appName: 'KuvikNotes',
            };
            this.authenticationService.registration(userInfo).subscribe(response => {
                // this.events.publish('note:login');
                console.log('User Created: ', response);
                this.router.navigateByUrl('/user/login');
            }, error => {
                this.toastMessageService.presentToast('Email or Password is not valid');
            });
        } else {
            console.log('Error while registering user');
            this.validateAllFormFields(this.formGroup);
            this.toastMessageService.presentToast('Please fill the form!!');
        }
    }

    isFieldValid(field: string) {
        return (!this.formGroup.get(field).valid || this.formGroup.get(field).untouched) &&
            (this.formSubmitAttempt);
    }

    displayFieldCss(field: string) {
        return {
            'has-error': this.isFieldValid(field),
            'has-feedback': this.isFieldValid(field)
        };
    }

    getErrorMessages(elementName) {
        for (const propertyName in this.formGroup.controls[elementName].errors) {
            if (this.formGroup.controls[elementName].errors.hasOwnProperty(propertyName) &&
                this.formGroup.controls[elementName].invalid) {
                const errorName = this.formGroup.controls[elementName].errors[propertyName];
                return this.validationService.getValidatorErrorMessage(propertyName, errorName);
            }
        }
        return null;
    }

    validateAllFormFields(formGroup: FormGroup) {
        Object.keys(formGroup.controls).forEach(field => {
            const control = formGroup.get(field);
            if (control instanceof FormControl) {
                control.markAsTouched({onlySelf: true});
            } else if (control instanceof FormGroup) {
                this.validateAllFormFields(control);
            }
        });
    }
}

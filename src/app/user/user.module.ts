import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {UserPage} from './user.page';
import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';
import {ForgetPasswordComponent} from './forget-password/forget-password.component';
import {ForgetUsernameComponent} from './forget-username/forget-username.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {SharedModule} from '../shared/shared.module';


const routes: Routes = [
    {
        path: '',
        component: UserPage
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'registration',
        component: RegistrationComponent
    },
    {
        path: 'forget-password',
        component: ForgetPasswordComponent
    },
    {
        path: 'forget-username',
        component: ForgetUsernameComponent
    },
    {
        path: 'reset-password/:token',
        component: ResetPasswordComponent
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        UserPage,
        LoginComponent,
        RegistrationComponent,
        ForgetPasswordComponent,
        ForgetUsernameComponent,
        ResetPasswordComponent
    ]
})
export class UserPageModule {
}

import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Events} from '@ionic/angular';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
    public formGroup: FormGroup;
    public passwordResetToken: any;

    constructor(private fb: FormBuilder,
                public route: ActivatedRoute,
                private router: Router,
                private events: Events,
                private authenticationService: AuthenticationService) {
        this.passwordResetToken = this.route.snapshot.params.token;
        console.log('Password reset token', this.passwordResetToken);
        this.formGroup = this.fb.group({
            password: ['', Validators.required],
            confirm: ['', Validators.required],
        }, {updateOn: 'submit'});
    }

    ngOnInit() {
    }

    Submit() {
        if (this.formGroup.valid) {
            const request = {
                password: this.formGroup.value.password,
                token: this.passwordResetToken,
                appType: 'web',
                appName: 'KuvikNotes'
            };
            this.authenticationService.resetPassword(request).subscribe(response => {
                this.router.navigateByUrl('/user/login');
            }, error => {
                // this.toastMessageProvider.presentToast('Email or Password is not valid');
                console.log('Password reset error', error);
            });
        } else {

        }
    }

    register() {
        this.router.navigateByUrl('/user/registration');
    }

    forgetPassword() {
        this.router.navigateByUrl('/user/forget-password');
    }

    reset() {
        console.log('Resetting the form');
        this.formGroup.reset();
    }
}

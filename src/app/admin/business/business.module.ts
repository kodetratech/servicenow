import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {BusinessPage} from './business.page';
import {BusinessCreateComponent} from './business-create/business-create.component';
import {BusinessListComponent} from './business-list/business-list.component';
import {BusinessViewComponent} from './business-view/business-view.component';

const routes: Routes = [
    {
        path: '',
        component: BusinessPage,
        children: [
            {
                path: 'list',
                component: BusinessListComponent
            },
            {
                path: 'create',
                component: BusinessCreateComponent
            },
            {
                path: 'view/:id',
                component: BusinessViewComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [BusinessPage, BusinessCreateComponent, BusinessListComponent, BusinessViewComponent]
})
export class BusinessPageModule {
}

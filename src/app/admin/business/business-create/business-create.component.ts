import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AdminService} from '../../../services/admin.service';
import {Device} from '@ionic-native/device/ngx';

@Component({
    selector: 'app-business-create',
    templateUrl: './business-create.component.html',
    styleUrls: ['./business-create.component.scss']
})
export class BusinessCreateComponent implements OnInit {

    public businessForm: FormGroup;

    constructor(private fb: FormBuilder,
                private device: Device,
                private adminService: AdminService,
                private router: Router) {
        this.businessForm = this.fb.group({
            firstName: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            lastName: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            email: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            phone: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(15)])],
            street1: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            street2: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            city: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            state: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            postalCode: ['', Validators.compose([Validators.minLength(5), Validators.maxLength(5)])],
            search: ['', Validators.compose([Validators.minLength(5), Validators.maxLength(500)])],
        });
    }

    ngOnInit() {
    }

    cancel() {

    }

    addBusiness() {
        console.log('create customer', this.businessForm.value);
        if (this.businessForm.valid) {
            const request = {
                firstName: this.businessForm.value.firstName,
                lastName: this.businessForm.value.lastName,
                email: this.businessForm.value.email,
                phone: this.businessForm.value.phone,
                role: 'business',
                address: {
                    street1: this.businessForm.value.street1,
                    street2: this.businessForm.value.street2,
                    city: this.businessForm.value.city,
                    state: this.businessForm.value.state,
                    postalCode: this.businessForm.value.postalCode,
                },
                deviceId: this.device.uuid
            };
            this.adminService.createBusinessUser(request).subscribe((response) => {
                console.log('Business User Created successfully', response);
            }, (error) => {
                console.log('Business User Created successfully', error);
            });
        } else {
            console.log('Business User Account Form Invalid');
        }

    }
}

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-business-list',
    templateUrl: './business-list.component.html',
    styleUrls: ['./business-list.component.scss']
})
export class BusinessListComponent implements OnInit {

    constructor(private router: Router,
                public route: ActivatedRoute) {
    }

    ngOnInit() {
    }

    newCustomer() {
        this.router.navigate(['/admin/business/create']);
    }
}

import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.page.html',
    styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {
    public appPages = [
        {
            title: 'Home',
            url: '/admin/main',
            icon: 'home'
        },
        {
            title: 'Business',
            url: '/admin/business/list',
            icon: 'build'
        },
        {
            title: 'Customer',
            url: '/home/customer/list',
            icon: 'person'
        }
    ];

    constructor() {
    }

    ngOnInit() {
    }

}

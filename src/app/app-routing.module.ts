import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'admin/main',
        pathMatch: 'full'
    },
    {
        path: 'home',
        loadChildren: './home/home.module#HomePageModule'
    },
    {
        path: 'user',
        loadChildren: './user/user.module#UserPageModule'
    },
    {
        path: 'admin',
        loadChildren: './admin/admin.module#AdminPageModule'
    },
  { path: 'agent', loadChildren: './agent/agent.module#AgentPageModule' },
  { path: 'business', loadChildren: './business/business.module#BusinessPageModule' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

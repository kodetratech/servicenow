import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
    selector: 'app-customer-create',
    templateUrl: './customer-create.component.html',
    styleUrls: ['./customer-create.component.scss']
})
export class CustomerCreateComponent implements OnInit {
    public customerForm: FormGroup;

    constructor(private fb: FormBuilder,
                private router: Router) {
        this.customerForm = this.fb.group({
            firstName: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            lastName: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            email: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            phone: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(15)])],
            street1: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            street2: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            city: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            state: ['', Validators.compose([Validators.minLength(2), Validators.maxLength(100)])],
            postalCode: ['', Validators.compose([Validators.minLength(5), Validators.maxLength(5)])],
        });
    }

    ngOnInit() {
    }

    cancel() {

    }

    addCustomer() {
        console.log('create customer', this.customerForm.value);
    }
}

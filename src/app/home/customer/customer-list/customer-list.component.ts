import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-customer-list',
    templateUrl: './customer-list.component.html',
    styleUrls: ['./customer-list.component.scss']
})
export class CustomerListComponent implements OnInit {

    constructor(private router: Router,
                public route: ActivatedRoute) {
    }

    ngOnInit() {
    }

    newCustomer() {
        this.router.navigate(['/home/customer/create']);
    }
}

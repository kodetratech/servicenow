import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {CustomerPage} from './customer.page';
import {CustomerListComponent} from './customer-list/customer-list.component';
import {CustomerCreateComponent} from './customer-create/customer-create.component';
import {CustomerViewComponent} from './customer-view/customer-view.component';

const routes: Routes = [
    {
        path: '',
        component: CustomerPage,
        children: [
            {
                path: 'list',
                component: CustomerListComponent
            },
            {
                path: 'create',
                component: CustomerCreateComponent
            },
            {
                path: 'view/:id',
                component: CustomerViewComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [CustomerPage, CustomerListComponent, CustomerCreateComponent, CustomerViewComponent]
})
export class CustomerPageModule {
}

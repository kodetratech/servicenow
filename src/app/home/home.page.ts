import {Component} from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {
    public appPages = [
        {
            title: 'Home',
            url: '/home/main',
            icon: 'home'
        },
        {
            title: 'Service',
            url: '/home/service/list',
            icon: 'build'
        },
        {
            title: 'Customer',
            url: '/home/customer/list',
            icon: 'person'
        }
    ];

}

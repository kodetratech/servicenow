import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {ServicePage} from './service.page';
import {ServiceListComponent} from './service-list/service-list.component';
import {ServiceCreateComponent} from './service-create/service-create.component';
import {ServiceViewComponent} from './service-view/service-view.component';

const routes: Routes = [
    {
        path: '',
        component: ServicePage,
        children: [
            {
                path: 'list',
                component: ServiceListComponent
            },
            {
                path: 'create',
                component: ServiceCreateComponent
            },
            {
                path: 'view/:id',
                component: ServiceViewComponent
            }
        ]
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild(routes)
    ],
    declarations: [ServicePage, ServiceListComponent, ServiceCreateComponent, ServiceViewComponent]
})
export class ServicePageModule {
}

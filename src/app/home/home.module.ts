import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';

import {HomePage} from './home.page';
import {DashboardComponent} from './dashboard/dashboard.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RouterModule.forChild([
            {
                path: '',
                component: HomePage,
                children: [
                    {
                        path: 'main',
                        component: DashboardComponent
                    },
                    {
                        path: 'service',
                        loadChildren: './service/service.module#ServicePageModule'
                    },
                    {
                        path: 'customer',
                        loadChildren: './customer/customer.module#CustomerPageModule'
                    }
                ]
            }
        ])
    ],
    declarations: [HomePage, DashboardComponent]
})
export class HomePageModule {
}

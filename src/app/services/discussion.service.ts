import {Injectable} from '@angular/core';
import {Dictionary} from '../util/helper/dictionary';
import {environment} from '../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {AuthenticationService} from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class DiscussionService {
  private forumTopics = new Dictionary<any>();
  private baseUrl = environment.hostName + environment.appUrl;
  public topicId: any;
  private dataSource = new BehaviorSubject<any>(this.forumTopics.Values());
  data = this.dataSource.asObservable();

  constructor(public http: HttpClient,
              private authenticationService: AuthenticationService) {
  }

  getHeader() {
    // console.log('authenticate toke',this.authenticationService.getToken());
    return new HttpHeaders().set('Authorization', 'Bearer ' + this.authenticationService.getToken());
  }

  setTopicId(notebookId) {
    this.topicId = notebookId;
  }

  getTopicId() {
    return this.topicId;
  }

  resetForumTopics() {
    this.forumTopics.reset();
  }


  saveForumTopicsDict(topicList) {
    this.forumTopics = new Dictionary<any>();
    topicList.map((note) => {
      this.forumTopics.Add(note._id, note);
    });
    return this.forumTopics.Values();
  }

  updateNoteInDictById(note) {
    this.forumTopics.Add(note._id, note);
    this.dataSource.next(this.forumTopics.Values());
    return this.forumTopics.Values();
  }

  deleteNoteFromDict(noteId) {
    this.forumTopics.Remove(noteId);
    return this.forumTopics.Values();
  }

  getTopicById(noteId) {
    return this.forumTopics.Item(noteId);
  }

  getTopics() {
    return this.forumTopics.Values();
  }

  getForumTopics(sort, direction, page): Observable<any> {
    return this.http.get(this.baseUrl + '/forum/topic', {
      headers: this.getHeader()
    });
  }

  createTopic(topic): Observable<any> {
    return this.http.post(this.baseUrl + '/forum/create/topic', topic, {
      headers: this.getHeader(),
      observe: 'response'
    })
      .map(this.extractData).catch(this.handleError);
  }

  private extractData(res: HttpResponse<any>) {
    // let headers = new Headers(res.headers);
    const response = {
      data: res.body.data,
      total: res.headers.get('X-Total-Docs'),
      limit: res.headers.get('X-Limit'),
      page: res.headers.get('X-Page'),
      pages: res.headers.get('X-Pages'),
    };
    return response;
  }

  private handleError(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(error);
  }
}

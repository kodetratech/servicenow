import {Injectable} from '@angular/core';
import {FormControl, ValidatorFn, Validators} from '@angular/forms';
import {Dictionary} from '../util/helper/dictionary';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {
  public regEx = new Dictionary<RegExp>();

  constructor() {
    this.regEx.Add('email', new RegExp('^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z]{2,4})$)'));
    this.regEx.Add('number', new RegExp('^\\d+$'));
    this.regEx.Add('word', new RegExp('^[a-zA-Z]{1}[a-zA-Z\\s\\-\\.\\/\(\)\\\']*$'));
    this.regEx.Add('alpha', new RegExp('^[a-zA-Z]+$'));
    this.regEx.Add('name', new RegExp('^[a-zA-Z]{1}[a-zA-ZàáâäãåąčćęèéêëėįìíîïñńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,\'-]+$'));
    this.regEx.Add('alphaNum', new RegExp('^[a-zA-Z0-9\\s\\.\\,\\#\\/\\-\\:\\&\\%\\\']+$'));
    this.regEx.Add('grade', new RegExp('^[a-zA-Z0-9\\-\\+\\%]+$'));
    this.regEx.Add('middleName', new RegExp('^[a-zA-Z]{1}[a-zA-Z\-\\\']*$'));
    this.regEx.Add('address', new RegExp('^[a-zA-Z0-9\\s\\.\\,\\#\\/\\-\\:\\&\\\']+$'));
    this.regEx.Add('city', new RegExp('^([a-zA-Z\u0080-\u024F]+(?:. |-| |\'))*[a-zA-Z\u0080-\u024F]*$'));
    this.regEx.Add('states', new RegExp('(aa|AE|AK|AL|AP|AR|AS|AZ|CA|CO|CT|DC|DE|FL|FM|GA|GU|HI|IA|ID|IL|IN|KS|KY|LA|MA|MD|ME|MH|MI|' +
      'MN|MO|MP|MS|MT|NC|ND|NE|NH|NJ|NM|NV|NY|OH|OK|OR|PA|PR|PW|RI|SC|SD|TN|TX|UT|VA|VI|VT|WA|WI|WV|WY)'));
    this.regEx.Add('zip', new RegExp('^\\d{5}$'));
    this.regEx.Add('postalcode', new RegExp('^[0-9]{5,6}$'));
    this.regEx.Add('phone', new RegExp('^\\([2-9]\\d{2}\\)\\s\\d{3}\\-\\d{4}$'));
    this.regEx.Add('ssn', new RegExp('\\b(?!000)(?!9)(?:[0-8]\\d{2})\\s\\-\\s\\d{2}(?!0000)\\s\\-\\s\\d{4}\\b'));
    this.regEx.Add('password', new RegExp('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!@#$%^+\\-\(\)])((?![<>"&\\\\/]).){7,50}$'));
    this.regEx.Add('dateOfBirth', new RegExp('^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$'));
    this.regEx.Add('youtube', new RegExp('^(https?\\:\\/\\/)?(www\\.)?(youtube\\.com|youtu\\.?be)\\/.+$'));

  }

  public getValidatorsFunctionArray(required: boolean, minLength: number, maxLength: number, pattern: string, customValidatorArray: Array<any>): Array<ValidatorFn> {
    const ValidatorsFunctionArray: Array<ValidatorFn> = [];
    if (required) {
      ValidatorsFunctionArray.push(Validators.required);
    }
    if (minLength && minLength != null && minLength > 0) {
      ValidatorsFunctionArray.push(Validators.minLength(minLength));
    }
    if (maxLength && maxLength != null && maxLength > 0) {
      ValidatorsFunctionArray.push(Validators.maxLength(maxLength));
    }
    if (pattern && pattern != null) {
      ValidatorsFunctionArray.push(Validators.pattern(this.regEx.Item(pattern)));
    }
    if (customValidatorArray) {
      for (let i = 0; i < customValidatorArray.length; i++) {
        ValidatorsFunctionArray.push(customValidatorArray[i]);
      }
    }
    return ValidatorsFunctionArray;

  }

  public getValidatorErrorMessage(validatorName: string, validatorValue?: any): string {
    const config = {
      'required': 'Required',
      'invalidCreditCard': 'Is invalid credit card number',
      'invalidEmailAddress': 'Invalid email address',
      'invalidPassword': 'Invalid password. Password must be at least 6 characters long, and contain a number.',
      'minlength': `Minimum length ${validatorValue.requiredLength}`,
      'maxlength': `Maximum length ${validatorValue.requiredLength}`,
      'pattern': 'Invalid Pattern',
      'productExist': 'Product Already Exist',
      'maxlimit5': `Maximum allowed limit is 5`
    };

    return config[validatorName];
  }

  public creditCardValidator(control) {
    // Visa, MasterCard, American Express, Diners Club, Discover, JCB
    if (control.value.match(/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/)) {
      return null;
    } else {
      return {'invalidCreditCard': true};
    }
  }

  public emailValidator(control: FormControl) {
    // RFC 2822 compliant regex
    if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
      return null;
    } else {
      return {'invalidEmailAddress': true};
    }
  }

  // public checkProductExist(control) {
  //   console.log('control vl');
  //   let productExist;
  //   this.productService.checkProductExist(control.value)
  //     .subscribe(result => {
  //         productExist = result.success;
  //       },
  //       error => {
  //         console.log("error Could Not saved product to database", error);
  //       });
  //   if (productExist) {
  //     return null;
  //   } else {
  //     return {'productExist': true};
  //   }
  // }

  public passwordValidator(control) {
    // {6,100}           - Assert password is between 6 and 100 characters
    // (?=.*[0-9])       - Assert a string has at least one number
    if (control.value.match(/^(?=.*[0-9])[a-zA-Z0-9!@#$%^&*]{6,100}$/)) {
      return null;
    } else {
      return {'invalidPassword': true};
    }
  }
}

import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantUtilService {

  constructor() {
  }

  getCountryList(name: string) {
    return [
      {name: name, value: ''},
      {name: 'India', value: 'India'},
      {name: 'USA', value: 'USA'},
      {name: 'United Kingdom', value: 'United Kingdom'},
      {name: 'United Arab Emirates', value: 'United Arab Emirates'},
      {name: 'Canada', value: 'Canada'},
      {name: 'Australia', value: 'Australia'},
      {name: 'New Zealand', value: 'New Zealand'},
      {name: 'Pakistan', value: 'Pakistan'},
      {name: 'Saudi Arabia', value: 'Saudi Arabia'},
      {name: 'Kuwait', value: 'Kuwait'},
      {name: 'South Africa', value: 'South Africa'}
    ];
  }

  getExpectedSalary() {
    const salary = [];
    let previousVal = 0;
    for (let i = 50000; i <= 5000000; i = i + 50000) {
      const val = {
        name: previousVal + ' USD' + ' - ' + i + ' USD'
      };
      salary.push(val);
      previousVal = i;
    }
    return salary;
  }

  getExperience() {
    const salary = [];
    let previousVal = 0;
    for (let i = 2; i <= 40; i = i + 2) {
      const val = {
        name: previousVal + ' Yrs' + ' - ' + i + ' Yrs'
      };
      salary.push(val);
      previousVal = i;
    }
    return salary;
  }

  getJobType = function () {
    return [
      {
        name: 'Full-time'
      },
      {
        name: 'Part-time'
      },
      {
        name: 'Contract'
      },
      {
        name: 'Temporary'
      },
      {
        name: 'Volunteer'
      },
      {
        name: 'Other'
      }
    ];
  };
  getJobLevel = function () {
    return [
      {
        name: 'Internship'
      },
      {
        name: 'Entry Level'
      },
      {
        name: 'Associate'
      },
      {
        name: 'Mid-Senior level'
      },
      {
        name: 'Director'
      },
      {
        name: 'Executive'
      },
      {
        name: 'Other'
      }
    ];
  };

  companyIndustryArray = function () {
    return ['Accounting',
      'Airlines/Aviation',
      'Alternative Dispute Resolution',
      'Alternative Medicine',
      'Animation',
      'Apparel & Fashion',
      'Architecture & Planning',
      'Arts and Crafts',
      'Automotive',
      'Aviation &amp; Aerospace',
      'Banking',
      'Biotechnology',
      'Broadcast Media',
      'Building Materials',
      'Business Supplies andEquipment',
      'Capital Markets',
      'Chemicals',
      'Civic &amp; Social Organization',
      'Civil Engineering',
      'Commercial Real Estate',
      'Computer &amp; Network Security',
      'Computer Games',
      'Computer Hardware',
      'Computer Networking',
      'Computer Software',
      'Construction',
      'Consumer Electronics',
      'Consumer Goods',
      'Cosmetics',
      'Dairy',
      'Defense &amp; Space',
      'Design',
      'Education Management',
      'E-Learning',
      'Electrical/Electronic Manufacturing',
      'Entertainment',
      'Environmental Services',
      'Events Services',
      'Executive Office',
      'Facilities Services',
      'Farming',
      'Financial Services',
      'Fine Art',
      'Fishery',
      'Food &amp; Beverages',
      'Food Production',
      'Fund-Raising',
      'Furniture',
      'Gambling &amp; Casinos',
      'Glass, Ceramics &amp; Concrete',
      'Government Administration',
      'Government Relations',
      'Graphic Design',
      'Health, Wellness and Fitness',
      'Higher Education',
      'Hospital &amp; Health Care',
      'Hospitality',
      'Human Resources',
      'Import and Export',
      'Individual &amp; Family Services',
      'Industrial Automation',
      'Information Services',
      'Information Technology and Services',
      'Insurance',
      'International Affairs',
      'International Trade and Development',
      'Internet',
      'Investment Banking',
      'Investment Management',
      'Judiciary',
      'Law Enforcement',
      'Law Practice',
      'Legal Services',
      'Legislative Office',
      'Leisure, Travel &amp; Tourism',
      'Libraries',
      'Logistics and Supply Chain',
      'Luxury Goods &amp; Jewelry',
      'Machinery',
      'Management Consulting',
      'Maritime',
      'Marketing and Advertising',
      'Market Research',
      'Mechanical or Industrial Engineering',
      'Media Production',
      'Medical Devices',
      'Medical Practice',
      'Mental Health Care',
      'Military',
      'Mining &amp; Metals',
      'Motion Pictures and Film',
      'Museums and Institutions',
      'Music',
      'Nanotechnology',
      'Newspapers',
      'Nonprofit Organization Management',
      'Oil &amp; Energy',
      'Online Media',
      ' Outsourcing/Offshoring',
      'Package/Freight Delivery',
      'Packaging and Containers',
      'Paper &amp; Forest Products',
      'Performing Arts',
      'Pharmaceuticals',
      'Philanthropy',
      'Photography',
      'Plastics',
      'Political Organization',
      'Primary/Secondary Education',
      'Printing',
      'Professional Training &amp; Coaching',
      'Program Development',
      'Public Policy',
      'Public Relations and Communications',
      'Public Safety',
      'Publishing',
      'Railroad Manufacture',
      'Ranching',
      'Real Estate',
      'Recreational Facilities and Services',
      'Religious Institutions',
      'Renewables &amp; Environment',
      'Research',
      'Restaurants',
      'Retail',
      'Security and Investigations',
      'Semiconductors',
      'Shipbuilding',
      'Sporting Goods',
      'Sports',
      'Staffing and Recruiting',
      'Supermarkets',
      'Telecommunications',
      'Textiles',
      'Think Tanks',
      'Tobacco',
      'Translation and Localization',
      'Transportation/Trucking/Railroad',
      'Utilities',
      'Venture Capital &amp; Private Equity',
      'Veterinary',
      'Warehousing',
      'Wholesale',
      'Wine and Spirits',
      'Wireless',
      'Writing and Editing'];
  };

  companyIndustry = function () {
    return [{
      name: 'Industry', value: ['Accounting',
        'Airlines/Aviation',
        'Alternative Dispute Resolution',
        'Alternative Medicine',
        'Animation',
        'Apparel & Fashion',
        'Architecture & Planning',
        'Arts and Crafts',
        'Automotive',
        'Aviation &amp; Aerospace',
        'Banking',
        'Biotechnology',
        'Broadcast Media',
        'Building Materials',
        'Business Supplies andEquipment',
        'Capital Markets',
        'Chemicals',
        'Civic &amp; Social Organization',
        'Civil Engineering',
        'Commercial Real Estate',
        'Computer &amp; Network Security',
        'Computer Games',
        'Computer Hardware',
        'Computer Networking',
        'Computer Software',
        'Construction',
        'Consumer Electronics',
        'Consumer Goods',
        'Cosmetics',
        'Dairy',
        'Defense &amp; Space',
        'Design',
        'Education Management',
        'E-Learning',
        'Electrical/Electronic Manufacturing',
        'Entertainment',
        'Environmental Services',
        'Events Services',
        'Executive Office',
        'Facilities Services',
        'Farming',
        'Financial Services',
        'Fine Art',
        'Fishery',
        'Food &amp; Beverages',
        'Food Production',
        'Fund-Raising',
        'Furniture',
        'Gambling &amp; Casinos',
        'Glass, Ceramics &amp; Concrete',
        'Government Administration',
        'Government Relations',
        'Graphic Design',
        'Health, Wellness and Fitness',
        'Higher Education',
        'Hospital &amp; Health Care',
        'Hospitality',
        'Human Resources',
        'Import and Export',
        'Individual &amp; Family Services',
        'Industrial Automation',
        'Information Services',
        'Information Technology and Services',
        'Insurance',
        'International Affairs',
        'International Trade and Development',
        'Internet',
        'Investment Banking',
        'Investment Management',
        'Judiciary',
        'Law Enforcement',
        'Law Practice',
        'Legal Services',
        'Legislative Office',
        'Leisure, Travel &amp; Tourism',
        'Libraries',
        'Logistics and Supply Chain',
        'Luxury Goods &amp; Jewelry',
        'Machinery',
        'Management Consulting',
        'Maritime',
        'Marketing and Advertising',
        'Market Research',
        'Mechanical or Industrial Engineering',
        'Media Production',
        'Medical Devices',
        'Medical Practice',
        'Mental Health Care',
        'Military',
        'Mining &amp; Metals',
        'Motion Pictures and Film',
        'Museums and Institutions',
        'Music',
        'Nanotechnology',
        'Newspapers',
        'Nonprofit Organization Management',
        'Oil &amp; Energy',
        'Online Media',
        ' Outsourcing/Offshoring',
        'Package/Freight Delivery',
        'Packaging and Containers',
        'Paper &amp; Forest Products',
        'Performing Arts',
        'Pharmaceuticals',
        'Philanthropy',
        'Photography',
        'Plastics',
        'Political Organization',
        'Primary/Secondary Education',
        'Printing',
        'Professional Training &amp; Coaching',
        'Program Development',
        'Public Policy',
        'Public Relations and Communications',
        'Public Safety',
        'Publishing',
        'Railroad Manufacture',
        'Ranching',
        'Real Estate',
        'Recreational Facilities and Services',
        'Religious Institutions',
        'Renewables &amp; Environment',
        'Research',
        'Restaurants',
        'Retail',
        'Security and Investigations',
        'Semiconductors',
        'Shipbuilding',
        'Sporting Goods',
        'Sports',
        'Staffing and Recruiting',
        'Supermarkets',
        'Telecommunications',
        'Textiles',
        'Think Tanks',
        'Tobacco',
        'Translation and Localization',
        'Transportation/Trucking/Railroad',
        'Utilities',
        'Venture Capital &amp; Private Equity',
        'Veterinary',
        'Warehousing',
        'Wholesale',
        'Wine and Spirits',
        'Wireless',
        'Writing and Editing']
    }];
  };
  getJobFunction = function () {
    return [{
      name: 'Industry', value: ['Accounting',
        'Airlines/Aviation',
        'Alternative Dispute Resolution',
        'Alternative Medicine',
        'Animation',
        'Apparel & Fashion',
        'Architecture & Planning',
        'Arts and Crafts',
        'Automotive',
        'Aviation &amp; Aerospace',
        'Banking',
        'Biotechnology',
        'Broadcast Media',
        'Building Materials',
        'Business Supplies andEquipment',
        'Capital Markets',
        'Chemicals',
        'Civic &amp; Social Organization',
        'Civil Engineering',
        'Commercial Real Estate',
        'Computer &amp; Network Security',
        'Computer Games',
        'Computer Hardware',
        'Computer Networking',
        'Computer Software',
        'Construction',
        'Consumer Electronics',
        'Consumer Goods',
        'Cosmetics',
        'Dairy',
        'Defense &amp; Space',
        'Design',
        'Education Management',
        'E-Learning',
        'Electrical/Electronic Manufacturing',
        'Entertainment',
        'Environmental Services',
        'Events Services',
        'Executive Office',
        'Facilities Services',
        'Farming',
        'Financial Services',
        'Fine Art',
        'Fishery',
        'Food &amp; Beverages',
        'Food Production',
        'Fund-Raising',
        'Furniture',
        'Gambling &amp; Casinos',
        'Glass, Ceramics &amp; Concrete',
        'Government Administration',
        'Government Relations',
        'Graphic Design',
        'Health, Wellness and Fitness',
        'Higher Education',
        'Hospital &amp; Health Care',
        'Hospitality',
        'Human Resources',
        'Import and Export',
        'Individual &amp; Family Services',
        'Industrial Automation',
        'Information Services',
        'Information Technology and Services',
        'Insurance',
        'International Affairs',
        'International Trade and Development',
        'Internet',
        'Investment Banking',
        'Investment Management',
        'Judiciary',
        'Law Enforcement',
        'Law Practice',
        'Legal Services',
        'Legislative Office',
        'Leisure, Travel &amp; Tourism',
        'Libraries',
        'Logistics and Supply Chain',
        'Luxury Goods &amp; Jewelry',
        'Machinery',
        'Management Consulting',
        'Maritime',
        'Marketing and Advertising',
        'Market Research',
        'Mechanical or Industrial Engineering',
        'Media Production',
        'Medical Devices',
        'Medical Practice',
        'Mental Health Care',
        'Military',
        'Mining &amp; Metals',
        'Motion Pictures and Film',
        'Museums and Institutions',
        'Music',
        'Nanotechnology',
        'Newspapers',
        'Nonprofit Organization Management',
        'Oil &amp; Energy',
        'Online Media',
        ' Outsourcing/Offshoring',
        'Package/Freight Delivery',
        'Packaging and Containers',
        'Paper &amp; Forest Products',
        'Performing Arts',
        'Pharmaceuticals',
        'Philanthropy',
        'Photography',
        'Plastics',
        'Political Organization',
        'Primary/Secondary Education',
        'Printing',
        'Professional Training &amp; Coaching',
        'Program Development',
        'Public Policy',
        'Public Relations and Communications',
        'Public Safety',
        'Publishing',
        'Railroad Manufacture',
        'Ranching',
        'Real Estate',
        'Recreational Facilities and Services',
        'Religious Institutions',
        'Renewables &amp; Environment',
        'Research',
        'Restaurants',
        'Retail',
        'Security and Investigations',
        'Semiconductors',
        'Shipbuilding',
        'Sporting Goods',
        'Sports',
        'Staffing and Recruiting',
        'Supermarkets',
        'Telecommunications',
        'Textiles',
        'Think Tanks',
        'Tobacco',
        'Translation and Localization',
        'Transportation/Trucking/Railroad',
        'Utilities',
        'Venture Capital &amp; Private Equity',
        'Veterinary',
        'Warehousing',
        'Wholesale',
        'Wine and Spirits',
        'Wireless',
        'Writing and Editing']
    }];
  };
  getHeight = function () {
    const height = [];
    for (let i = 4; i < 8; i++) {
      for (let j = 0; j < 12; j++) {
        if (j === 0) {
          height.push(i + 'ft');
        } else {
          height.push(i + ' feet' + '-' + j + ' inch');
        }
      }
    }
    return [{
      name: '',
      value: height
    }];
  };
  getHeightShortForm = function () {
    const height = [];
    for (let i = 4; i < 8; i++) {
      for (let j = 0; j < 12; j++) {
        if (j === 0) {
          height.push(i + 'ft');
        } else {
          height.push(i + ' \"' + '-' + j + ' \'');
        }
      }
    }
    return [{
      name: '',
      value: height
    }];
  };
  getHeightInCentimeter = function () {
    const height = [];
    for (let i = 4; i < 8; i++) {
      for (let j = 0; j < 12; j++) {
        height.push((((i * 12) + j) * 2.54).toFixed(2) + 'cm');
      }
    }
    return [{
      name: '',
      value: height
    }];
  };
  getWeightInKgs = function () {
    const weight = [];
    let i = 45;
    while (i < 200) {
      const j = i + 5;
      const tempWight = i + '-' + j + ' Kgs';
      weight.push(tempWight);
      i = j;
    }
    return [{
      name: '',
      value: weight
    }];
  };
  getWeightInPounds = function () {
    const weight = [];
    let i = 45;
    while (i < 200) {
      const j = i + 5;
      const tempWight = (i * 2.2).toFixed(0) + '-' + (j * 2.2).toFixed(0) + ' Lbs';
      weight.push(tempWight);
      i = j;
    }
    return [{
      name: '',
      value: weight
    }];
  };


  getLanguages = function () {
    return [
      {
        name: 'North',
        value: ['Hindi-Delhi', 'Hindi-MP', 'Hindi-UP', 'Punjabi', 'Bihari', 'Rajasthani/Marwari',
          'Haryanvi', 'Himachali', 'Kashmi', 'Sindhi', 'Urdu']
      },
      {
        name: 'East',
        value: ['Bengali', 'Oriya', 'Assamese', 'Aurnachali', 'Manipuri', 'Khasi/Meghalaya',
          'Mizo', 'Konyak/Nagaland', 'Sikkim/ Nepali', 'Tripuri']
      },
      {
        name: 'West',
        value: ['Marathi', 'Gujarati/Kutchi', 'Hindi-MP', 'Konkani', 'Sindhi', 'Daman & Diu']
      },
      {
        name: 'South',
        value: ['Tamil', 'Telugu', 'Kannada', 'Malayalam', 'Nicobarese', 'Lakshadweep/Mahl',
          'Tulu', 'Urdu']
      },
      {
        name: 'others',
        value: ['English', 'Foriegn origin']
      }
    ];
  };

  getSect = function () {
    return [
      {
        name: 'Please Select',
        value: ['Arya Samaji', 'Radha Soami', 'Brahmo Samaj', 'Shaivism(Shiva Follower)', 'Vaishnav(Vishnu follower)']
      }
    ];
  };

  getComplexion = function () {
    return [
      {
        name: '',
        value: ['Very Fair', 'Fair', 'Wheatish', 'Wheatish Brown', 'Dark']
      }
    ];
  };

  getSkillsArray = function () {
    return ['ASP',
      'C Programming',
      'Java',
      'Javascript',
      'Visual Basic',
      'Script Install',
      'System Admin',
      'Ruby on Rails',
      'AJAX',
      'Joomla',
      'Shopping Carts',
      'Game Design',
      'Social Networking',
      'Magento',
      'Web Scraping',
      'MySpace',
      'Drupal',
      'C# Programming',
      'Erlang',
      'JavaFX',
      'Django',
      'User Interface / IA',
      'Software Architecture',
      'Oracle',
      'Microsoft Access',
      'Asterisk PBX',
      'Google App Engine',
      'Social Engine',
      'PayPal API',
      'Cocoa',
      'Mac OS',
      'Sharepoint',
      'Metatrader',
      'Forum Software',
      'Software Testing',
      'Embedded Software',
      'Zen Cart',
      'AS400 &amp; iSeries',
      'Apache',
      'REALbasic',
      'Microsoft Exchange',
      'Active Directory',
      'CakePHP',
      'Protoshare',
      'Balsamiq',
      'Golang',
      'Virtual Worlds',
      'SAP',
      'Chordiant',
      'Azure',
      'CUDA',
      'Google Analytics',
      'Prestashop',
      'Fortran',
      'Apache Solr',
      'Pentaho',
      'Usability Testing',
      'Amazon Web Services',
      'C++ Programming',
      'Solaris',
      'FreelancerAPI',
      'FileMaker',
      'PICK Multivalue DB',
      'Blog Install',
      'Google Earth',
      'Virtuemart',
      'Sencha / YahooUI',
      'TaoBao API',
      'Business Catalyst',
      'CRE Loaded',
      'Visual Foxpro',
      'CubeCart',
      'AutoHotkey',
      'Parallels Desktop',
      'Parallels Automation',
      'LabVIEW',
      'TestStand',
      'Dynamics',
      'Smarty PHP',
      'eLearning',
      'Apple Safari',
      'Face Recognition',
      'Pattern Matching',
      'Computer Graphics',
      'Haskell',
      'Dart',
      'Gamification',
      'Assembly',
      'Website Management',
      'Game Consoles',
      'Database Administration',
      'Agile Development',
      'Grease Monkey',
      'CS-Cart',
      'Big Data',
      'Hadoop',
      'Map Reduce',
      'Analytics',
      'SugarCRM',
      'Visual Basic for Apps',
      'x86/x64 Assembler',
      'Windows API',
      'Open Cart',
      'PhoneGap',
      'QuickBase',
      'Umbraco',
      'Biztalk',
      'Chef Configuration Management',
      'Scala',
      'backbone.js',
      'R Programming Language',
      'CasperJS',
      'Software Development',
      'Debian',
      'MariaDB',
      'Red Hat',
      'Jabber',
      'VMware',
      'Game Development',
      'Laravel',
      'Database Programming',
      'Network Administration',
      'ASP.NET',
      'Google Webmaster Tools',
      'Angular.js',
      'Angular 2 or 4',
      'Angular',
      'Database Development',
      'Salesforce App Development',
      'Mobile App Testing',
      'Data Warehousing',
      'Elasticsearch',
      'Call Control XML',
      'IBM Websphere Transformation Tool',
      'SPSS Statistics',
      'Qualtrics Survey Platform',
      'Argus Monitoring Software',
      'Parallax Scrolling',
      'Growth Hacking',
      'GameSalad',
      'Alibaba',
      'Adobe Air',
      'Snapchat',
      'Instagram',
      'React.js',
      'WatchKit',
      'Binary Analysis',
      'Google Maps API',
      'LESS/Sass/SCSS',
      'HBase',
      'Yarn',
      'Cassandra',
      'Spark',
      'OpenBravo',
      'Augmented Reality',
      'Vuforia',
      'Google Cardboard',
      'Magic Leap',
      'Android Wear SDK',
      'Samsung Accessory SDK',
      'iBeacon',
      'Tizen SDK for Wearables',
      'Leap Motion SDK',
      'AMQP',
      'Papiamento',
      'Squarespace',
      'Steam API',
      'Ionic Framework',
      'Tableau',
      'DataLife Engine',
      'Minitab',
      'Paytrace',
      'Lua',
      'Applescript',
      'Apache Ant',
      'Artificial Intelligence',
      'Google Cloud Storage',
      'OAuth',
      'OpenStack',
      'Regular Expressions',
      'Squid Cache',
      'Varnish Cache',
      'Veeam',
      'Titanium',
      'RapidWeaver',
      'Tally Definition Language',
      'Adobe Captivate',
      'PEGA PRPC',
      'JD Edwards CNC',
      'Open Journal Systems',
      'XPages',
      'Adobe Premiere Pro',
      'Email Developer',
      'App Developer',
      'Programming',
      'Storage Area Networks',
      'Grails',
      'Xamarin',
      'Raspberry Pi',
      'OpenSceneGraph',
      'Ray-tracing',
      'Parallel Processing',
      'Visualization',
      'Blockchain',
      'Learning Management Systems (LMS)',
      'Penetration Testing',
      'SEO Auditing',
      'Asana',
      'Facebook API',
      'Geographical Information System (GIS)',
      'Drawing',
      'Corel Draw',
      'Swing (Java)',
      'Adobe Illustrator',
      'Virtual Machines',
      'Camtasia',
      'Netbeans',
      'Pascal',
      'phpMyAdmin',
      'Digital Marketing',
      'CV Library',
      'Cinematography',
      'Adobe Muse',
      'Object Oriented Programming (OOP)',
      'Web Crawling',
      'Twitter API',
      'Instagram API',
      'Scrapy',
      'XAML',
      'Social Media Management',
      'ADO.NET',
      'Apache Maven',
      'Website Analytics',
      'Graphics Programming',
      'Bash Scripting',
      'Payment Gateway Integration',
      'Adobe Freehand',
      'API',
      'Translation',
      'Research',
      'Proofreading',
      'Financial Research',
      'Technical Writing',
      'Article Writing',
      'Research Writing',
      'Medical Writing',
      'Cartography &amp; Maps',
      'Travel Writing',
      'Grant Writing',
      'Article Rewriting',
      'LaTeX',
      'Press Releases',
      'Proposal/Bid Writing',
      'Wikipedia',
      'Communications',
      'Slogans',
      'Creative Writing',
      'Catch Phrases',
      'Apple iBooks Author',
      'Essay Writing',
      'Academic Writing',
      'Content Strategy',
      'Editorial Writing',
      'Adobe Flash',
      'Graphic Design',
      'Banner Design',
      'Photography',
      'Audio Services',
      'Industrial Design',
      'Illustrator',
      'Building Architecture',
      'Typography',
      'Animation',
      'Arts &amp; Crafts',
      'Fashion Design',
      'Adobe InDesign',
      'Adobe Dreamweaver',
      'Illustration',
      'After Effects',
      'Maya',
      'Stationery Design',
      'Voice Talent',
      'Corporate Identity',
      'Caricature &amp; Cartoons',
      'QuarkXPress',
      'Covers &amp; Packaging',
      'Format &amp; Layout',
      'Templates',
      'Business Cards',
      'Video Broadcasting',
      'Flash 3D',
      'Capture NX2',
      'ActionScript',
      'Finale / Sibelius',
      'Commercials',
      'Advertisement Design',
      '3ds Max',
      'Shopify Templates',
      'Final Cut Pro',
      '3D Animation',
      'Invitation Design',
      'Yahoo! Store Design',
      'Infographics',
      'Presentations',
      'Visual Arts',
      'Fashion Modeling',
      'Motion Graphics',
      'Videography',
      'Landing Pages',
      'Adobe LiveCycle Designer',
      'Makerbot',
      'Autodesk Revit',
      'Bootstrap',
      'Creative Design',
      'Audio Production',
      'Label Design',
      'Package Design',
      'GarageBand',
      'Apple Logic Pro',
      'Apple Compressor',
      'Apple Motion',
      'Cinema 4D',
      'Autodesk Inventor',
      'Tattoo Design',
      'Adobe Lightroom',
      'Vectorization',
      'Vehicle Signage',
      'Axure',
      'Wireframes',
      'Flow Charts',
      'Concept Art',
      'Adobe Fireworks',
      'eLearning Designer',
      'App Designer',
      'Book Artist',
      'Filmmaking',
      'User Interface Design',
      'Tekla Structures',
      'Storyboard',
      '3D Model Maker',
      'Image Processing',
      'Business Card Design',
      'Corel Painter',
      'Flash Animation',
      'Autodesk Sketchbook Pro',
      'Photo Restoration',
      '2D Animation',
      'Instagram Marketing',
      'Kinetic Typography',
      'Data Processing',
      'Data Entry',
      'Transcription',
      'Virtual Assistant',
      'Video Upload',
      'Web Search',
      'Technical Support',
      'Article Submission',
      'General Office',
      'Telephone Handling',
      'Time Management',
      'Call Center',
      'Email Handling',
      'Investment Research',
      'Qualitative Research',
      'Excel VBA',
      'Data Analytics',
      'Excel Macros',
      'Data Cleansing',
      'Data Scraping',
      'Data Extraction',
      'Matlab and Mathematica',
      'CAD/CAM',
      'Scientific Research',
      'Algorithm',
      'Statistics',
      'PLC &amp; SCADA',
      'Mechanical Engineering',
      'Medical',
      'Electrical Engineering',
      'Materials Engineering',
      'Chemical Engineering',
      'Structural Engineering',
      'Mechatronics',
      'Finite Element Analysis',
      'Quantum',
      'Cryptography',
      'PCB Layout',
      'AutoCAD',
      'Machine Learning',
      'Natural Language',
      'Manufacturing Design',
      'Mathematics',
      'Aeronautical Engineering',
      'Data Mining',
      'Climate Sciences',
      'Instrumentation',
      'Product Management',
      'Microstation',
      'Imaging',
      'Astrophysics',
      'Aerospace Engineering',
      'Human Sciences',
      'Arduino',
      'Broadcast Engineering',
      'Engineering Drawing',
      'Linear Programming',
      'Nanotechnology',
      'Industrial Engineering',
      'Telecommunications Engineering',
      'Clean Technology',
      'Geospatial',
      'Digital Design',
      'Wolfram',
      'Statistical Analysis',
      'Renewable Energy Design',
      'Data Science',
      'FPGA',
      'Surfboard Design',
      'Geotechnical Engineering',
      'Agronomy',
      'CATIA',
      'Neural Networks',
      'Very-large-scale integration (VLSI)',
      'Internet Marketing',
      'Telemarketing',
      'Sales',
      'Google Adwords',
      'Facebook Marketing',
      'Marketing',
      'Bulk Marketing',
      'Branding',
      'Advertising',
      'Leads',
      'eBay',
      'Google Adsense',
      'Market Research',
      'Classifieds Posting',
      'Ad Planning &amp; Buying',
      'Viral Marketing',
      'Affiliate Marketing',
      'Email Marketing',
      'Social Media Marketing',
      'Internet Research',
      'Search Engine Marketing',
      'Conversion Rate Optimisation',
      'Airbnb',
      'Mailchimp',
      'Mailwizz',
      'Journalist',
      'Visual Merchandising',
      'Pardot',
      'PPC Marketing',
      'Marketing Strategy',
      'Content Marketing',
      'Sales Promotion',
      'Brand Management',
      'Brand Marketing',
      'Twitter Marketing',
      'Keyword Research',
      'Legal',
      'Project Management',
      'Accounting',
      'Human Resources',
      'Business Plans',
      'Management',
      'Finance',
      'Tax',
      'Contracts',
      'Legal Research',
      'Patents',
      'Business Analysis',
      'Payroll',
      'Inventory Management',
      'Event Planning',
      'Public Relations',
      'Audit',
      'Salesforce.com',
      'Property Law',
      'Employment Law',
      'Tax Law',
      'SAS',
      'Visa / Immigration',
      'Fundraising',
      'Personal Development',
      'Property Management',
      'Startups',
      'Compliance',
      'Risk Management',
      'Financial Analysis',
      'Legal Writing',
      'Autotask',
      'Crystal Reports',
      'Linnworks Order Management',
      'Attorney',
      'Organizational Change Management',
      'Manufacturing',
      'Mobile App Development',
      'Android',
      'Symbian',
      'Blackberry',
      'Palm',
      'Nokia',
      'Amazon Kindle',
      'iPad',
      'Samsung',
      'Geolocation',
      'Appcelerator Titanium',
      'Amazon Fire',
      'Apple Watch',
      'Virtualization',
      'Afrikaans',
      'Indonesian',
      'Malay',
      'Catalan',
      'Danish',
      'German',
      'Spanish',
      'Spanish (Spain)',
      'Basque',
      'French (Canadian)',
      'Korean',
      'Croatian',
      'Italian',
      'Lithuanian',
      'Hungarian',
      'Japanese',
      'Norwegian',
      'Portuguese (Brazil)',
      'Romanian',
      'Russian',
      'Slovakian',
      'Slovenian',
      'Thai',
      'Vietnamese',
      'Simplified Chinese (China)',
      'Traditional Chinese (Taiwan)',
      'Traditional Chinese (Hong Kong)',
      'Bulgarian',
      'Serbian',
      'Arabic',
      'Bengali',
      'Punjabi',
      'Tamil',
      'Malayalam',
      'Kannada',
      'Albanian',
      'Latvian',
      'Macedonian',
      'Bosnian',
      'English Grammar',
      'Ukrainian',
      'Maltese',
      'Estonian',
      'Dari',
      'Voice Artist',
      'House Cleaning',
      'Commercial Cleaning',
      'Furniture Assembly',
      'Carwashing',
      'Food Takeaway',
      'Disposals',
      'Bathroom',
      'Carpentry',
      'Painting',
      'Electricians',
      'Landscaping &amp; Gardening',
      'Handyman',
      'Drafting',
      'Air Conditioning',
      'Gardening',
      'Pavement',
      'Excavation',
      'Lawn Mowing',
      'Appliance Installation',
      'Antenna Services',
      'Appliance Repair',
      'Asbestos Removal',
      'Asphalt',
      'Attic Access Ladders',
      'Awnings',
      'Balustrading',
      'Bamboo Flooring',
      'Brackets',
      'Bricklaying',
      'Building Consultants',
      'Carpet Repair &amp; Laying',
      'Carports',
      'Cement Bonding Agents',
      'Cleaning Carpet',
      'Cleaning Domestic',
      'Cleaning Upholstery',
      'Coating Materials',
      'Damp Proofing',
      'Drains',
      'Extensions &amp; Additions',
      'Financial Planning',
      'Floor Coatings',
      'Frames &amp; Trusses',
      'Gas Fitting',
      'Glass / Mirror &amp; Glazing',
      'Heating Systems',
      'Home Automation',
      'Hot Water',
      'IKEA Installation',
      'Landscaping',
      'Mortgage Brokering',
      'Removalist',
      'Cooking / Baking',
      'Laundry and Ironing',
      'Yard Work &amp; Removal',
      'Packing &amp; Shipping',
      'Event Staffing',
      'Decoration',
      'Home Organization',
      'Installation',
      'General Labor',
      'Hair Styles',
      'Make Up',
      'Mural Painting',
      'Landscape Design',
      'Training',
      'Testing / QA',
      'Freelance',
      'Education &amp; Tutoring',
      'Insurance',
      'Automotive',
      'Genealogy',
      'Test Automation',
      'Financial Markets',
      'Dating',
      'Health',
      'Anything Goes',
      'Valuation &amp; Appraisal',
      'Pattern Making',
      'Real Estate',
      'Brain Storming',
      'Flashmob',
      'Christmas',
      'Life Coaching',
      'Business Coaching',
      'Assembly',
      'Adobe Pagemaker'];
  };
  getSkills = function () {
    return [
      {name: 'ASP'},
      {name: 'C Programming'},
      {name: 'Java'},
      {name: 'Javascript'},
      {name: 'Visual Basic'},
      {name: 'Script Install'},
      {name: 'System Admin'},
      {name: 'Ruby on Rails'},
      {name: 'AJAX'},
      {name: 'Joomla'},
      {name: 'Shopping Carts'},
      {name: 'Game Design'},
      {name: 'Social Networking'},
      {name: 'Magento'},
      {name: 'Web Scraping'},
      {name: 'MySpace'},
      {name: 'Drupal'},
      {name: 'C# Programming'},
      {name: 'Erlang'},
      {name: 'JavaFX'},
      {name: 'Django'},
      {name: 'User Interface / IA'},
      {name: 'Software Architecture'},
      {name: 'Oracle'},
      {name: 'Microsoft Access'},
      {name: 'Asterisk PBX'},
      {name: 'Google App Engine'},
      {name: 'Social Engine'},
      {name: 'PayPal API'},
      {name: 'Cocoa'},
      {name: 'Mac OS'},
      {name: 'Sharepoint'},
      {name: 'Metatrader'},
      {name: 'Forum Software'},
      {name: 'Software Testing'},
      {name: 'Embedded Software'},
      {name: 'Zen Cart'},
      {name: 'AS400 &amp; iSeries'},
      {name: 'Apache'},
      {name: 'REALbasic'},
      {name: 'Microsoft Exchange'},
      {name: 'Active Directory'},
      {name: 'CakePHP'},
      {name: 'Protoshare'},
      {name: 'Balsamiq'},
      {name: 'Golang'},
      {name: 'Virtual Worlds'},
      {name: 'SAP'},
      {name: 'Chordiant'},
      {name: 'Azure'},
      {name: 'CUDA'},
      {name: 'Google Analytics'},
      {name: 'Prestashop'},
      {name: 'Fortran'},
      {name: 'Apache Solr'},
      {name: 'Pentaho'},
      {name: 'Usability Testing'},
      {name: 'Amazon Web Services'},
      {name: 'C++ Programming'},
      {name: 'Solaris'},
      {name: 'FreelancerAPI'},
      {name: 'FileMaker'},
      {name: 'PICK Multivalue DB'},
      {name: 'Blog Install'},
      {name: 'Google Earth'},
      {name: 'Virtuemart'},
      {name: 'Sencha / YahooUI'},
      {name: 'TaoBao API'},
      {name: 'Business Catalyst'},
      {name: 'CRE Loaded'},
      {name: 'Visual Foxpro'},
      {name: 'CubeCart'},
      {name: 'AutoHotkey'},
      {name: 'Parallels Desktop'},
      {name: 'Parallels Automation'},
      {name: 'LabVIEW'},
      {name: 'TestStand'},
      {name: 'Dynamics'},
      {name: 'Smarty PHP'},
      {name: 'eLearning'},
      {name: 'Apple Safari'},
      {name: 'Face Recognition'},
      {name: 'Pattern Matching'},
      {name: 'Computer Graphics'},
      {name: 'Haskell'},
      {name: 'Dart'},
      {name: 'Gamification'},
      {name: 'Assembly'},
      {name: 'Website Management'},
      {name: 'Game Consoles'},
      {name: 'Database Administration'},
      {name: 'Agile Development'},
      {name: 'Grease Monkey'},
      {name: 'CS-Cart'},
      {name: 'Big Data'},
      {name: 'Hadoop'},
      {name: 'Map Reduce'},
      {name: 'Analytics'},
      {name: 'SugarCRM'},
      {name: 'Visual Basic for Apps'},
      {name: 'x86/x64 Assembler'},
      {name: 'Windows API'},
      {name: 'Open Cart'},
      {name: 'PhoneGap'},
      {name: 'QuickBase'},
      {name: 'Umbraco'},
      {name: 'Biztalk'},
      {name: 'Chef Configuration Management'},
      {name: 'Scala'},
      {name: 'backbone.js'},
      {name: 'R Programming Language'},
      {name: 'CasperJS'},
      {name: 'Software Development'},
      {name: 'Debian'},
      {name: 'MariaDB'},
      {name: 'Red Hat'},
      {name: 'Jabber'},
      {name: 'VMware'},
      {name: 'Game Development'},
      {name: 'Laravel'},
      {name: 'Database Programming'},
      {name: 'Network Administration'},
      {name: 'ASP.NET'},
      {name: 'Google Webmaster Tools'},
      {name: 'Angular.js'},
      {name: 'Database Development'},
      {name: 'Salesforce App Development'},
      {name: 'Mobile App Testing'},
      {name: 'Data Warehousing'},
      {name: 'Elasticsearch'},
      {name: 'Call Control XML'},
      {name: 'IBM Websphere Transformation Tool'},
      {name: 'SPSS Statistics'},
      {name: 'Qualtrics Survey Platform'},
      {name: 'Argus Monitoring Software'},
      {name: 'Parallax Scrolling'},
      {name: 'Growth Hacking'},
      {name: 'GameSalad'},
      {name: 'Alibaba'},
      {name: 'Adobe Air'},
      {name: 'Snapchat'},
      {name: 'Instagram'},
      {name: 'React.js'},
      {name: 'WatchKit'},
      {name: 'Binary Analysis'},
      {name: 'Google Maps API'},
      {name: 'LESS/Sass/SCSS'},
      {name: 'HBase'},
      {name: 'Yarn'},
      {name: 'Cassandra'},
      {name: 'Spark'},
      {name: 'OpenBravo'},
      {name: 'Augmented Reality'},
      {name: 'Vuforia'},
      {name: 'Google Cardboard'},
      {name: 'Magic Leap'},
      {name: 'Android Wear SDK'},
      {name: 'Samsung Accessory SDK'},
      {name: 'iBeacon'},
      {name: 'Tizen SDK for Wearables'},
      {name: 'Leap Motion SDK'},
      {name: 'AMQP'},
      {name: 'Papiamento'},
      {name: 'Squarespace'},
      {name: 'Steam API'},
      {name: 'Ionic Framework'},
      {name: 'Tableau'},
      {name: 'DataLife Engine'},
      {name: 'Minitab'},
      {name: 'Paytrace'},
      {name: 'Lua'},
      {name: 'Applescript'},
      {name: 'Apache Ant'},
      {name: 'Artificial Intelligence'},
      {name: 'Google Cloud Storage'},
      {name: 'OAuth'},
      {name: 'OpenStack'},
      {name: 'Regular Expressions'},
      {name: 'Squid Cache'},
      {name: 'Varnish Cache'},
      {name: 'Veeam'},
      {name: 'Titanium'},
      {name: 'RapidWeaver'},
      {name: 'Tally Definition Language'},
      {name: 'Adobe Captivate'},
      {name: 'PEGA PRPC'},
      {name: 'JD Edwards CNC'},
      {name: 'Open Journal Systems'},
      {name: 'XPages'},
      {name: 'Adobe Premiere Pro'},
      {name: 'Email Developer'},
      {name: 'App Developer'},
      {name: 'Programming'},
      {name: 'Storage Area Networks'},
      {name: 'Grails'},
      {name: 'Xamarin'},
      {name: 'Raspberry Pi'},
      {name: 'OpenSceneGraph'},
      {name: 'Ray-tracing'},
      {name: 'Parallel Processing'},
      {name: 'Visualization'},
      {name: 'Blockchain'},
      {name: 'Learning Management Systems (LMS)'},
      {name: 'Penetration Testing'},
      {name: 'SEO Auditing'},
      {name: 'Asana'},
      {name: 'Facebook API'},
      {name: 'Geographical Information System (GIS)'},
      {name: 'Drawing'},
      {name: 'Corel Draw'},
      {name: 'Swing (Java)'},
      {name: 'Adobe Illustrator'},
      {name: 'Virtual Machines'},
      {name: 'Camtasia'},
      {name: 'Netbeans'},
      {name: 'Pascal'},
      {name: 'phpMyAdmin'},
      {name: 'Digital Marketing'},
      {name: 'CV Library'},
      {name: 'Cinematography'},
      {name: 'Adobe Muse'},
      {name: 'Object Oriented Programming (OOP)'},
      {name: 'Web Crawling'},
      {name: 'Twitter API'},
      {name: 'Instagram API'},
      {name: 'Scrapy'},
      {name: 'XAML'},
      {name: 'Social Media Management'},
      {name: 'ADO.NET'},
      {name: 'Apache Maven'},
      {name: 'Website Analytics'},
      {name: 'Graphics Programming'},
      {name: 'Bash Scripting'},
      {name: 'Payment Gateway Integration'},
      {name: 'Adobe Freehand'},
      {name: 'API'},
      {name: 'Translation'},
      {name: 'Research'},
      {name: 'Proofreading'},
      {name: 'Financial Research'},
      {name: 'Technical Writing'},
      {name: 'Article Writing'},
      {name: 'Research Writing'},
      {name: 'Medical Writing'},
      {name: 'Cartography &amp; Maps'},
      {name: 'Travel Writing'},
      {name: 'Grant Writing'},
      {name: 'Article Rewriting'},
      {name: 'LaTeX'},
      {name: 'Press Releases'},
      {name: 'Proposal/Bid Writing'},
      {name: 'Wikipedia'},
      {name: 'Communications'},
      {name: 'Slogans'},
      {name: 'Creative Writing'},
      {name: 'Catch Phrases'},
      {name: 'Apple iBooks Author'},
      {name: 'Essay Writing'},
      {name: 'Academic Writing'},
      {name: 'Content Strategy'},
      {name: 'Editorial Writing'},
      {name: 'Adobe Flash'},
      {name: 'Graphic Design'},
      {name: 'Banner Design'},
      {name: 'Photography'},
      {name: 'Audio Services'},
      {name: 'Industrial Design'},
      {name: 'Illustrator'},
      {name: 'Building Architecture'},
      {name: 'Typography'},
      {name: 'Animation'},
      {name: 'Arts &amp; Crafts'},
      {name: 'Fashion Design'},
      {name: 'Adobe InDesign'},
      {name: 'Adobe Dreamweaver'},
      {name: 'Illustration'},
      {name: 'After Effects'},
      {name: 'Maya'},
      {name: 'Stationery Design'},
      {name: 'Voice Talent'},
      {name: 'Corporate Identity'},
      {name: 'Caricature &amp; Cartoons'},
      {name: 'QuarkXPress'},
      {name: 'Covers &amp; Packaging'},
      {name: 'Format &amp; Layout'},
      {name: 'Templates'},
      {name: 'Business Cards'},
      {name: 'Video Broadcasting'},
      {name: 'Flash 3D'},
      {name: 'Capture NX2'},
      {name: 'ActionScript'},
      {name: 'Finale / Sibelius'},
      {name: 'Commercials'},
      {name: 'Advertisement Design'},
      {name: '3ds Max'},
      {name: 'Shopify Templates'},
      {name: 'Final Cut Pro'},
      {name: '3D Animation'},
      {name: 'Invitation Design'},
      {name: 'Yahoo! Store Design'},
      {name: 'Infographics'},
      {name: 'Presentations'},
      {name: 'Visual Arts'},
      {name: 'Fashion Modeling'},
      {name: 'Motion Graphics'},
      {name: 'Videography'},
      {name: 'Landing Pages'},
      {name: 'Adobe LiveCycle Designer'},
      {name: 'Makerbot'},
      {name: 'Autodesk Revit'},
      {name: 'Bootstrap'},
      {name: 'Creative Design'},
      {name: 'Audio Production'},
      {name: 'Label Design'},
      {name: 'Package Design'},
      {name: 'GarageBand'},
      {name: 'Apple Logic Pro'},
      {name: 'Apple Compressor'},
      {name: 'Apple Motion'},
      {name: 'Cinema 4D'},
      {name: 'Autodesk Inventor'},
      {name: 'Tattoo Design'},
      {name: 'Adobe Lightroom'},
      {name: 'Vectorization'},
      {name: 'Vehicle Signage'},
      {name: 'Axure'},
      {name: 'Wireframes'},
      {name: 'Flow Charts'},
      {name: 'Concept Art'},
      {name: 'Adobe Fireworks'},
      {name: 'eLearning Designer'},
      {name: 'App Designer'},
      {name: 'Book Artist'},
      {name: 'Filmmaking'},
      {name: 'User Interface Design'},
      {name: 'Tekla Structures'},
      {name: 'Storyboard'},
      {name: '3D Model Maker'},
      {name: 'Image Processing'},
      {name: 'Business Card Design'},
      {name: 'Corel Painter'},
      {name: 'Flash Animation'},
      {name: 'Autodesk Sketchbook Pro'},
      {name: 'Photo Restoration'},
      {name: '2D Animation'},
      {name: 'Instagram Marketing'},
      {name: 'Kinetic Typography'},
      {name: 'Data Processing'},
      {name: 'Data Entry'},
      {name: 'Transcription'},
      {name: 'Virtual Assistant'},
      {name: 'Video Upload'},
      {name: 'Web Search'},
      {name: 'Technical Support'},
      {name: 'Article Submission'},
      {name: 'General Office'},
      {name: 'Telephone Handling'},
      {name: 'Time Management'},
      {name: 'Call Center'},
      {name: 'Email Handling'},
      {name: 'Investment Research'},
      {name: 'Qualitative Research'},
      {name: 'Excel VBA'},
      {name: 'Data Analytics'},
      {name: 'Excel Macros'},
      {name: 'Data Cleansing'},
      {name: 'Data Scraping'},
      {name: 'Data Extraction'},
      {name: 'Matlab and Mathematica'},
      {name: 'CAD/CAM'},
      {name: 'Scientific Research'},
      {name: 'Algorithm'},
      {name: 'Statistics'},
      {name: 'PLC &amp; SCADA'},
      {name: 'Mechanical Engineering'},
      {name: 'Medical'},
      {name: 'Electrical Engineering'},
      {name: 'Materials Engineering'},
      {name: 'Chemical Engineering'},
      {name: 'Structural Engineering'},
      {name: 'Mechatronics'},
      {name: 'Finite Element Analysis'},
      {name: 'Quantum'},
      {name: 'Cryptography'},
      {name: 'PCB Layout'},
      {name: 'AutoCAD'},
      {name: 'Machine Learning'},
      {name: 'Natural Language'},
      {name: 'Manufacturing Design'},
      {name: 'Mathematics'},
      {name: 'Aeronautical Engineering'},
      {name: 'Data Mining'},
      {name: 'Climate Sciences'},
      {name: 'Instrumentation'},
      {name: 'Product Management'},
      {name: 'Microstation'},
      {name: 'Imaging'},
      {name: 'Astrophysics'},
      {name: 'Aerospace Engineering'},
      {name: 'Human Sciences'},
      {name: 'Arduino'},
      {name: 'Broadcast Engineering'},
      {name: 'Engineering Drawing'},
      {name: 'Linear Programming'},
      {name: 'Nanotechnology'},
      {name: 'Industrial Engineering'},
      {name: 'Telecommunications Engineering'},
      {name: 'Clean Technology'},
      {name: 'Geospatial'},
      {name: 'Digital Design'},
      {name: 'Wolfram'},
      {name: 'Statistical Analysis'},
      {name: 'Renewable Energy Design'},
      {name: 'Data Science'},
      {name: 'FPGA'},
      {name: 'Surfboard Design'},
      {name: 'Geotechnical Engineering'},
      {name: 'Agronomy'},
      {name: 'CATIA'},
      {name: 'Neural Networks'},
      {name: 'Very-large-scale integration (VLSI)'},
      {name: 'Internet Marketing'},
      {name: 'Telemarketing'},
      {name: 'Sales'},
      {name: 'Google Adwords'},
      {name: 'Facebook Marketing'},
      {name: 'Marketing'},
      {name: 'Bulk Marketing'},
      {name: 'Branding'},
      {name: 'Advertising'},
      {name: 'Leads'},
      {name: 'eBay'},
      {name: 'Google Adsense'},
      {name: 'Market Research'},
      {name: 'Classifieds Posting'},
      {name: 'Ad Planning &amp; Buying'},
      {name: 'Viral Marketing'},
      {name: 'Affiliate Marketing'},
      {name: 'Email Marketing'},
      {name: 'Social Media Marketing'},
      {name: 'Internet Research'},
      {name: 'Search Engine Marketing'},
      {name: 'Conversion Rate Optimisation'},
      {name: 'Airbnb'},
      {name: 'Mailchimp'},
      {name: 'Mailwizz'},
      {name: 'Journalist'},
      {name: 'Visual Merchandising'},
      {name: 'Pardot'},
      {name: 'PPC Marketing'},
      {name: 'Marketing Strategy'},
      {name: 'Content Marketing'},
      {name: 'Sales Promotion'},
      {name: 'Brand Management'},
      {name: 'Brand Marketing'},
      {name: 'Twitter Marketing'},
      {name: 'Keyword Research'},
      {name: 'Legal'},
      {name: 'Project Management'},
      {name: 'Accounting'},
      {name: 'Human Resources'},
      {name: 'Business Plans'},
      {name: 'Management'},
      {name: 'Finance'},
      {name: 'Tax'},
      {name: 'Contracts'},
      {name: 'Legal Research'},
      {name: 'Patents'},
      {name: 'Business Analysis'},
      {name: 'Payroll'},
      {name: 'Inventory Management'},
      {name: 'Event Planning'},
      {name: 'Public Relations'},
      {name: 'Audit'},
      {name: 'Salesforce.com'},
      {name: 'Property Law'},
      {name: 'Employment Law'},
      {name: 'Tax Law'},
      {name: 'SAS'},
      {name: 'Visa / Immigration'},
      {name: 'Fundraising'},
      {name: 'Personal Development'},
      {name: 'Property Management'},
      {name: 'Startups'},
      {name: 'Compliance'},
      {name: 'Risk Management'},
      {name: 'Financial Analysis'},
      {name: 'Legal Writing'},
      {name: 'Autotask'},
      {name: 'Crystal Reports'},
      {name: 'Linnworks Order Management'},
      {name: 'Attorney'},
      {name: 'Organizational Change Management'},
      {name: 'Manufacturing'},
      {name: 'Mobile App Development'},
      {name: 'Android'},
      {name: 'Symbian'},
      {name: 'Blackberry'},
      {name: 'Palm'},
      {name: 'Nokia'},
      {name: 'Amazon Kindle'},
      {name: 'iPad'},
      {name: 'Samsung'},
      {name: 'Geolocation'},
      {name: 'Appcelerator Titanium'},
      {name: 'Amazon Fire'},
      {name: 'Apple Watch'},
      {name: 'Virtualization'},
      {name: 'Afrikaans'},
      {name: 'Indonesian'},
      {name: 'Malay'},
      {name: 'Catalan'},
      {name: 'Danish'},
      {name: 'German'},
      {name: 'Spanish'},
      {name: 'Spanish (Spain)'},
      {name: 'Basque'},
      {name: 'French (Canadian)'},
      {name: 'Korean'},
      {name: 'Croatian'},
      {name: 'Italian'},
      {name: 'Lithuanian'},
      {name: 'Hungarian'},
      {name: 'Japanese'},
      {name: 'Norwegian'},
      {name: 'Portuguese (Brazil)'},
      {name: 'Romanian'},
      {name: 'Russian'},
      {name: 'Slovakian'},
      {name: 'Slovenian'},
      {name: 'Thai'},
      {name: 'Vietnamese'},
      {name: 'Simplified Chinese (China)'},
      {name: 'Traditional Chinese (Taiwan)'},
      {name: 'Traditional Chinese (Hong Kong)'},
      {name: 'Bulgarian'},
      {name: 'Serbian'},
      {name: 'Arabic'},
      {name: 'Bengali'},
      {name: 'Punjabi'},
      {name: 'Tamil'},
      {name: 'Malayalam'},
      {name: 'Kannada'},
      {name: 'Albanian'},
      {name: 'Latvian'},
      {name: 'Macedonian'},
      {name: 'Bosnian'},
      {name: 'English Grammar'},
      {name: 'Ukrainian'},
      {name: 'Maltese'},
      {name: 'Estonian'},
      {name: 'Dari'},
      {name: 'Voice Artist'},
      {name: 'House Cleaning'},
      {name: 'Commercial Cleaning'},
      {name: 'Furniture Assembly'},
      {name: 'Carwashing'},
      {name: 'Food Takeaway'},
      {name: 'Disposals'},
      {name: 'Bathroom'},
      {name: 'Carpentry'},
      {name: 'Painting'},
      {name: 'Electricians'},
      {name: 'Landscaping &amp; Gardening'},
      {name: 'Handyman'},
      {name: 'Drafting'},
      {name: 'Air Conditioning'},
      {name: 'Gardening'},
      {name: 'Pavement'},
      {name: 'Excavation'},
      {name: 'Lawn Mowing'},
      {name: 'Appliance Installation'},
      {name: 'Antenna Services'},
      {name: 'Appliance Repair'},
      {name: 'Asbestos Removal'},
      {name: 'Asphalt'},
      {name: 'Attic Access Ladders'},
      {name: 'Awnings'},
      {name: 'Balustrading'},
      {name: 'Bamboo Flooring'},
      {name: 'Brackets'},
      {name: 'Bricklaying'},
      {name: 'Building Consultants'},
      {name: 'Carpet Repair &amp; Laying'},
      {name: 'Carports'},
      {name: 'Cement Bonding Agents'},
      {name: 'Cleaning Carpet'},
      {name: 'Cleaning Domestic'},
      {name: 'Cleaning Upholstery'},
      {name: 'Coating Materials'},
      {name: 'Damp Proofing'},
      {name: 'Drains'},
      {name: 'Extensions &amp; Additions'},
      {name: 'Financial Planning'},
      {name: 'Floor Coatings'},
      {name: 'Frames &amp; Trusses'},
      {name: 'Gas Fitting'},
      {name: 'Glass / Mirror &amp; Glazing'},
      {name: 'Heating Systems'},
      {name: 'Home Automation'},
      {name: 'Hot Water'},
      {name: 'IKEA Installation'},
      {name: 'Landscaping'},
      {name: 'Mortgage Brokering'},
      {name: 'Removalist'},
      {name: 'Cooking / Baking'},
      {name: 'Laundry and Ironing'},
      {name: 'Yard Work &amp; Removal'},
      {name: 'Packing &amp; Shipping'},
      {name: 'Event Staffing'},
      {name: 'Decoration'},
      {name: 'Home Organization'},
      {name: 'Installation'},
      {name: 'General Labor'},
      {name: 'Hair Styles'},
      {name: 'Make Up'},
      {name: 'Mural Painting'},
      {name: 'Landscape Design'},
      {name: 'Training'},
      {name: 'Testing / QA'},
      {name: 'Freelance'},
      {name: 'Education &amp; Tutoring'},
      {name: 'Insurance'},
      {name: 'Automotive'},
      {name: 'Genealogy'},
      {name: 'Test Automation'},
      {name: 'Financial Markets'},
      {name: 'Dating'},
      {name: 'Health'},
      {name: 'Anything Goes'},
      {name: 'Valuation &amp; Appraisal'},
      {name: 'Pattern Making'},
      {name: 'Real Estate'},
      {name: 'Brain Storming'},
      {name: 'Flashmob'},
      {name: 'Christmas'},
      {name: 'Life Coaching'},
      {name: 'Business Coaching'},
      {name: 'Assembla'},
      {name: 'Adobe Pagemaker'},
    ];
  };

  getYesNo(name: string) {
    return [{name: name, value: ''}, {name: 'Yes', value: 'yes'}, {name: 'no', value: 'No'},];
  }

  getAnnualIncome = function () {
    return [
      {
        name: '',
        value: ['No Income', 'Rs. 0 - 1 Lakh', 'Rs. 1 - 2 Lakh', 'Rs. 2 - 3 Lakh',
          'Rs. 3 - 4 Lakh', 'Rs. 4 - 5 Lakh', 'Rs. 5 - 6 Lakh', 'Rs. 6 - 7 Lakh',
          'Rs. 7 - 8 Lakh', 'Rs. 8 - 9 Lakh', 'Rs. 9 - 10 Lakh', 'Rs. 10 - 15 Lakh',
          'Rs. 15 - 20 Lakh', 'Rs. 20 - 25 Lakh', 'Rs. 25 - 35 Lakh', 'Rs. 35 - 50 Lakh',
          'Rs. 50 - 70 Lakh', 'Rs. 70 Lakh - 1 Crore', 'Rs. 1 Crore & Above']
      }
    ];
  };

  getRelationship = function () {
    return [
      {
        name: 'Relationship',
        value: ['Father', 'Mother', 'Brother', 'Sister', 'Grand Father', 'Grand Mother', 'Uncle', 'Aunt']
      }
    ];
  };

  getSunSign = function () {
    return [{
      name: '',
      value: ['Aries', 'Taurus', 'Gemini', 'Cancer', 'Leo', 'Virgo', 'Libra', 'Scorpio', 'Sagittarius', 'Capricorn', 'Aquarius', 'Pisces']
    }];
  };
  getMoonSign = function () {
    return [{
      name: '',
      value: ['Mesh', 'Vrishabh', 'Mithun', 'Kark', 'Simha', 'Kanya', 'Tula', 'Vrishchick', 'Dhanu', 'Makar', 'Kumbh', 'Meen']
    }];
  };


  getNakshatra = function () {
    return [{
      name: '',
      value: ['Ashwini',
        'Bharani',
        'Krittika',
        'Rohini',
        'Mrigasira',
        'Ardra',
        'Punarvasu',
        'Pusya',
        'Aslesa',
        'Magha',
        'Purva Phalguni',
        'Uttara Phalguni',
        'Hasta',
        'Chitra',
        'Swati',
        'Vishakha',
        'Anuradha',
        'Jyestha',
        'Mula',
        'Purva Ashadha',
        'Uttara Ashadha',
        'Shravana',
        'Dhanistha',
        'Shatabhishak',
        'Purva Bhadrapada',
        'Uttara Bhadrapada',
        'Revati']
    }];
  };
  getHighestEducation = function () {
    return [
      {
        name: 'Engineering',
        value: ['B.Arch', 'B.E/B.Tech', 'B.Pharma', 'M.Arch', 'M.E/M.Tech', 'M.Pharma', 'M.S.(Engineering)']
      },
      {
        name: 'Computers',
        value: ['B.IT', 'BCA', 'MCA/PGDCA']
      },
      {
        name: 'Finance/Commerce',
        value: ['B.Com', 'CA', 'CFA',
          'CS', 'ICWA', 'M.Com']
      },
      {
        name: 'Management',
        value: ['BBA', 'BHM', 'MBA/PGDM']
      },
      {
        name: 'Medicine',
        value: ['BAMS', 'BDS', 'BHMS', 'BVSc.', 'M.D.', 'MDS', 'M.S. (Medicine)', 'MBBS', 'MVSc.']
      },
      {
        name: 'Law',
        value: ['BL/LLB', 'ML/LLM']
      },
      {
        name: 'Arts/Science',
        value: ['B.A', 'B.Ed', 'B.Sc', 'M.A', 'M.Ed', 'M.Sc']
      },
      {
        name: 'Doctorate',
        value: ['M.Phil', 'Ph.D']
      },
      {
        name: 'Non-Graduate',
        value: ['High School', 'Trade School', 'Diploma']
      },
      {
        name: 'Others',
        value: ['Other']
      }
    ];
  };
  getAge = function () {
    const age = [];
    for (let i = 18; i < 71; i++) {
      age.push(i);
    }
    return [{
      name: '',
      value: age
    }];
  };
  getCountries = function () {
    return [
      {
        name: 'Mostly Used',
        value: ['India', 'USA', 'United Kingdom', 'United Arab Emirates', 'Canada', 'Australia', 'New Zealand',
          'Pakistan', 'Saudi Arabia', 'Kuwait', 'South Africa'
        ]
      },
      {
        name: 'A',
        value: [
          'Afghanistan',
          'Albania',
          'Algeria',
          'American Samoa',
          'Andorra',
          'Angola',
          'Anguilla',
          'Antigua & Barbuda',
          'Argentina',
          'Armenia',
          'Australia',
          'Austria',
          'Azerbaijan'
        ]
      },
      {
        name: 'B',
        value: [
          'Bahamas',
          'Bahrain',
          'Bangladesh',
          'Barbados',
          'Belarus',
          'Belgium',
          'Belize',
          'Bermuda',
          'Bhutan',
          'Bolivia',
          'Bosnia and Herzegovina',
          'Botswana',
          'Brazil',
          'Brunei',
          'Bulgaria',
          'Burkina Faso',
          'Burundi'
        ]
      },
      {
        name: 'C',
        value: ['Cambodia',
          'Cameroon',
          'Canada',
          'Cape Verde',
          'Cayman Islands',
          'Central African Republic',
          'Chad',
          'Chile',
          'China',
          'Colombia',
          'Comoros',
          'Congo (DRC)',
          'Congo',
          'Cook Islands',
          'Costa Rica',
          'Cote d\'Ivoire',
          'Croatia (Hrvatska)',
          'Cuba',
          'Cyprus',
          'Czech Republic']
      },
      {
        name: 'D',
        value: ['Denmark',
          'Djibouti',
          'Dominica',
          'Dominican Republic']
      },
      {
        name: 'E',
        value: ['East Timor',
          'Ecuador',
          'Egypt',
          'El Salvador',
          'Equatorial Guinea',
          'Eritrea',
          'Estonia',
          'Ethiopia']
      },
      {
        name: 'F',
        value: ['Falkland Islands',
          'Faroe Islands',
          'Fiji Islands',
          'Finland',
          'France',
          'French Guiana',
          'French Polynesia']
      },
      {
        name: 'G',
        value: ['Gabon',
          'Gambia',
          'Georgia',
          'Germany',
          'Ghana',
          'Gibraltar',
          'Greece',
          'Greenland',
          'Grenada',
          'Guadeloupe',
          'Guam',
          'Guatemala',
          'Guinea',
          'Guinea-Bissau',
          'Guyana']
      },
      {
        name: 'H',
        value: ['Haiti',
          'Honduras',
          'Hong Kong SAR',
          'Hungary']
      },
      {
        name: 'I',
        value: ['Iceland',
          'India',
          'Indonesia',
          'Iran',
          'Iraq',
          'Ireland',
          'Israel',
          'Italy']
      },
      {
        name: 'J',
        value: ['Jamaica',
          'Japan',
          'Jordan']
      },
      {
        name: 'K',
        value: ['Kazakhstan',
          'Kenya',
          'Kiribati',
          'Korea',
          'Kuwait',
          'Kyrgyzstan']
      },
      {
        name: 'L',
        value: ['Laos',
          'Latvia',
          'Lebanon',
          'Lesotho',
          'Liberia',
          'Libya',
          'Liechtenstein',
          'Lithuania',
          'Luxembourg']
      },
      {
        name: 'M',
        value: [
          'Macao SAR',
          'Macedonia',
          'Madagascar',
          'Malawi',
          'Malaysia',
          'Maldives',
          'Mali',
          'Malta',
          'Martinique',
          'Mauritania',
          'Mauritius',
          'Mayotte',
          'Mexico',
          'Micronesia',
          'Moldova',
          'Monaco',
          'Mongolia',
          'Montserrat',
          'Morocco',
          'Mozambique',
          'Myanmar']
      },
      {
        name: 'N',
        value: [
          'Namibia',
          'Nauru',
          'Nepal',
          'Netherlands Antilles',
          'Netherlands',
          'New Caledonia',
          'New Zealand',
          'Nicaragua',
          'Niger',
          'Nigeria',
          'Niue',
          'Norfolk Island',
          'North Korea',
          'Norway']
      },
      {
        name: 'O',
        value: ['Oman']
      },
      {
        name: 'P',
        value: ['Pakistan',
          'Panama',
          'Papua New Guinea',
          'Paraguay',
          'Peru',
          'Philippines',
          'Pitcairn Islands',
          'Poland',
          'Portugal',
          'Puerto Rico']
      },
      {
        name: 'Q',
        value: ['Qatar']
      },
      {
        name: 'R',
        value: ['Reunion',
          'Romania',
          'Russia',
          'Rwanda']
      },
      {
        name: 'S',
        value: [
          'Samoa',
          'San Marino',
          'Sao Tome and Principe',
          'Saudi Arabia',
          'Senegal',
          'Serbia and Montenegro',
          'Seychelles',
          'Sierra Leone',
          'Singapore',
          'Slovakia',
          'Slovenia',
          'Solomon Islands',
          'Somalia',
          'South Africa',
          'Spain',
          'Sri Lanka',
          'St. Helena',
          'St. Kitts and Nevis',
          'St. Lucia',
          'St. Pierre and Miquelon',
          'St. Vincent &amp; Grenadines',
          'Sudan',
          'Suriname',
          'Swaziland',
          'Sweden',
          'Switzerland',
          'Syria']
      },
      {
        name: 'T',
        value: [
          'Taiwan',
          'Tajikistan',
          'Tanzania',
          'Thailand',
          'Togo',
          'Tokelau',
          'Tonga',
          'Trinidad and Tobago',
          'Tunisia',
          'Turkey',
          'Turkmenistan',
          'Turks and Caicos Islands',
          'Tuvalu']
      },
      {
        name: 'U',
        value: [
          'Uganda',
          'Ukraine',
          'United Arab Emirates',
          'United Kingdom',
          'Uruguay',
          'USA',
          'Uzbekistan']
      },
      {
        name: 'V',
        value: [
          'Vanuatu',
          'Venezuela',
          'Vietnam',
          'Virgin Islands (British)',
          'Virgin Islands']
      },
      {
        name: 'W',
        value: ['Wallis and Futuna']
      },
      {
        name: 'Y',
        value: ['Yemen', 'Yugoslavia']
      },
      {
        name: 'Z',
        value: ['Zambia', 'Zimbabwe']
      }
    ];

  };
  getCaste = function () {
    return [
      {
        name: 'A',
        value: ['Ad Dharmi', 'Adi Andhra', 'Adi Dravida', 'Adi Karnataka', 'Agamudayar', 'Aggarwal', 'Agri', 'Ahir', 'Ahom', 'Ambalavasi', 'Arora',
          'Arunthathiyar', 'Arya Vysya']
      },
      {
        name: 'B',
        value: ['Baghel/Gaderiya', 'Baidya', 'Baishnab', 'Baishya', 'Balija', 'Balija Naidu', 'Bania', 'Banik', 'Banjara',
          'Bari', 'Barujibi', 'Besta', 'Bhandari', 'Bhatia', 'Bhatraju', 'Bhavsar', 'Bhovi/Bhoi', 'Billava', 'Bishnoi/Vishnoi', 'Boyer',
          'Brahmbatt', 'Brahmin', 'Brahmin  Niyogi', 'Brahmin Anavil', 'Brahmin Audichya', 'Brahmin Bajkhedwal', 'Brahmin Barendra', 'Brahmin Bhargava',
          'Brahmin Bhatt', 'Brahmin Bhumihar', 'Brahmin Brahacharanam', 'Brahmin Daivadnya', 'Brahmin Deshastha', 'Brahmin Dhiman', 'Brahmin Dravida', 'Brahmin Dunua',
          'Brahmin Garhwali', 'Brahmin Gaud Saraswat (GSB)', 'Brahmin Gaur', 'Brahmin Goswami', 'Brahmin Gujar Gaur', 'Brahmin Gurukkal', 'Brahmin Halua', 'Brahmin Havyaka',
          'Brahmin Hoysala', 'Brahmin Iyengar', 'Brahmin Iyer', 'Brahmin Jangid', 'Brahmin Jangra', 'Brahmin Jhadua', 'Brahmin Jhijhotiya', 'Brahmin Jogi', 'Brahmin Kanyakubj',
          'Brahmin Karhade', 'Brahmin Kashmiri Pandit', 'Brahmin Koknastha', 'Brahmin Kota', 'Brahmin Kulin', 'Brahmin Kumaoni', 'Brahmin Maithil', 'Brahmin Modh', 'Brahmin Mohyal', 'Brahmin Nagar', 'Brahmin Namboodiri', 'Brahmin Narmadiya', 'Brahmin Panda', 'Brahmin Pandit', 'Brahmin Pareek', 'Brahmin Pushkarna', 'Brahmin Rarhi',
          'Brahmin Rigvedi', 'Brahmin Rudraj', 'Brahmin Sakaldwipi', 'Brahmin Sanadya', 'Brahmin Sanketi', 'Brahmin Saraswat', 'Brahmin Saryuparin', 'Brahmin Shivalli', 'Brahmin Shrimali',
          'Brahmin Smartha', 'Brahmin Sri Vishnava', 'Brahmin Tyagi', 'Brahmin Vaidiki', 'Brahmin Viswa', 'Brahmin Vyas', 'Brahmin Yajurvedi', 'Brahmo', 'Bunt/Shetty']
      },
      {
        name: 'C',
        value: ['Chamar', 'Chambhar', 'Chandravanshi Kahar', 'Chasa', 'Chattada Sri Vaishnava', 'Chaudary', 'Chaurasia', 'Chettiar', 'Chhetri', 'CKP', 'Coorgi']
      },
      {
        name: 'D',
        value: ['Deshastha Maratha', 'Devadigas', 'Devang Koshthi', 'Devanga', 'Devendra Kula Vellalar', 'Dhangar', 'Dheevara', 'Dhoba', 'Dhobi', 'Dusadh']
      },

      {
        name: 'E',
        value: ['Edigas', 'Ezhava', 'Ezhuthachan']
      },
      {
        name: 'G',
        value: ['Gabit', 'Ganiga', 'Garhwali', 'Gavali', 'Gavara', 'Ghumar', 'Goala', 'Goan', 'Gomantak Maratha', 'Gondhali', 'Goud', 'Gounder', 'Gowda', 'Gramani', 'Gudia', 'Gujjar',
          'Gupta', 'Gurav']
      },

      {
        name: 'H',
        value: ['Hegde']
      },

      {
        name: 'J',
        value: ['Jaiswal', 'Jangam', 'Jat', 'Jatav']
      },
      {
        name: 'K',
        value: ['Kadava patel', 'Kahar', 'Kaibarta', 'Kalal', 'Kalar', 'Kalinga Vysya', 'Kalwar', 'Kamboj', 'Kamma',
          'Kansari', 'Kapol', 'Kapu', 'Kapu Munnuru', 'Karana', 'Karmakar', 'Karuneegar', 'Kasar', 'Kashyap',
          'Kayastha', 'Khandayat', 'Khandelwal', 'Kharwar', 'Khatik', 'Khatri', 'Kokanastha Maratha', 'Koli', 'Koli Mahadev', 'Kongu Vellala Gounder',
          'Konkani', 'Kori', 'Koshti', 'Kshatriya', 'Kshatriya Agnikula', 'Kudumbi', 'Kulalar', 'Kulita',
          'Kumawat', 'Kumbhakar', 'Kumhar/Kumbhar', 'Kummari', 'Kunbi', 'Kurmi', 'Kurmi kshatriya', 'Kuruba', 'Kuruhina shetty',
          'Kurumbar', 'Kushwaha', 'Kutchi', 'Kutchi Gurjar']
      },

      {
        name: 'L',
        value: ['Lambadi', 'Leva Patidar', 'Leva Patil', 'Lingayat', 'Lodhi Rajput', 'Lohana', 'Lohar', 'Lubana']
      },
      {
        name: 'M',
        value: ['Madiga', 'Mahajan', 'Mahar', 'Maheshwari', 'Mahindra', 'Mahisya', 'Majabi/Mazhbi', 'Mala', 'Mali',
          'Mallah', 'Manipuri', 'Mapila', 'Maratha', 'Maravar', 'Maruthuvar', 'Marwari', 'Matang', 'Mathur', 'Maurya', 'Meena',
          'Meenavar', 'Mehra', 'Menon', 'Meru', 'Meru darji', 'Modak', 'Mogaveera', 'Monchi',
          'Motati Reddy', 'Mudaliar', 'Mudaliar Arcot', 'Mudiraj', 'Muthuraja']
      },
      {
        name: 'N',
        value: ['Nadar', 'Naicker', 'Naidu', 'Naik/Nayak/Nayaka', 'Nair', 'Nair Veluthedathu', 'Nair Vilakkithala', 'Namasudra/Namosudra', 'Nambiar', 'Namboodiri', 'Napit', 'Nayee (Barber)', 'Nepali', 'Nhavi',]
      },

      {
        name: 'O',
        value: ['OBC', 'Oswal']
      },
      {
        name: 'P',
        value: ['Padmashali', 'Pal', 'Panchal', 'Panchamsali', 'Pandaram', 'Panicker', 'Parkava Kulam', 'Pasi', 'Patel', 'Patel Dodia',
          'Patel Kadva', 'Patel Leva', 'Patil', 'Patnaick', 'Patra', 'Perika', 'Pillai', 'Prajapati']
      },
      {
        name: 'R',
        value: ['Raigar', 'Rajaka', 'Rajbhar', 'Rajbonshi', 'Rajput', 'Rajput Garhwali', 'Rajput Kumaoni', 'Rajput Negi', 'Rajput Rohella/Tank', 'Ramdasia', 'Ramgarhia', 'Ravidasia', 'Rawat', 'Reddy']
      },
      {
        name: 'S',
        value: ['Sadgope', 'Saha', 'Sahu', 'Saini', 'Saliya', 'Scheduled Caste', 'Scheduled Tribe', 'Senai Thalaivar', 'Senguntha Mudaliyar', 'Settibalija', 'Shah', 'Shimpi', 'Sindhi', 'Sindhi Amil', 'Sindhi Baibhand', 'Sindhi Bhatia', 'Sindhi Larai', 'Sindhi Larkana', 'Sindhi Rohiri', 'Sindhi Sahiti',
          'Sindhi Sakkhar', 'Sindhi Shikarpuri', 'SKP', 'Somvanshi', 'Somvanshi Kayastha Prabhu', 'Sonar', 'Soni', 'Sood', 'Sourashtra',
          'Sozhiya Vellalar', 'Srisayani', 'SSK', 'Subarna Banik', 'Sundhi', 'Sutar', 'Swakula sali', 'Swarnkar']
      },

      {
        name: 'T',
        value: ['Tamboli', 'Tanti', 'Tantuway', 'Telaga', 'Teli', 'Thakkar',
          'Thakur', 'Thevar/Mukkulathor', 'Thigala', 'Thiyya', 'Tili', 'Togata',
          'Tonk Kshatriya', 'Tribe', 'Turupu Kapu']
      },

      {
        name: 'U',
        value: ['Uppara']
      },
      {
        name: 'V',
        value: ['Vaddera', 'Vaidiki Velanadu', 'Vaish', 'Vaishnav', 'Vaishnav Vanik', 'Vaishnava', 'Vaishya', 'Vaishya Vani', 'Valluvar',
          'Valmiki', 'Vania', 'Vaniya', 'Vanjari', 'Vankar', 'Vannar', 'Vannia Kula Kshatriyar', 'Vanniyar', 'Varshney',
          'Veershaiva/Veera Saivam', 'Velama', 'Velan', 'Vellalar', 'Vettuva Gounder', 'Vishwakarma', 'Vokkaliga', 'Vysya']
      },
      {
        name: 'Y',
        value: ['Yadav/Yadava']
      }
    ];
  };
  getOccupation = function () {
    return [{
      name: '',
      value: [
        'Looking for a job', 'working', 'Actor/Model', 'Advertising Professional', 'Agent',
        'Agriculture/Dairy', 'Air Hostess', 'Architect', 'Banking Professional', 'Beautician',
        'BPO/ITES', 'Businessperson', 'Civil Services (IAS/ IFS/ IPS/ IRS)', 'Consultant',
        'Corporate Communication', 'Corporate Planning Professional', 'Customer Services', 'Defence', 'Doctor', 'Education Professional',
        'Engineer - Non IT', 'Export/Import', 'Fashion Designer', 'Financial Services/Accounting', 'Fitness Professional',
        'Govt. Services', 'Hardware/Telecom', 'Healthcare Professional', 'Hotels/Hospitality Professional', 'Professional', 'Interior Designer',
        'Journalist', 'Lawyer/Legal Professional', 'Logistics/SCM Professional', 'Marketing Professional', 'Media Professional', ' Merchant Navy',
        'NGO/Social Services', 'Nurse', 'Pilot', 'Police', 'Printing/Packaging', 'Professor/Lecturer', 'Project Manager - IT', 'Project Manager - Non IT',
        'Research Professional', 'Restaurateur', 'Retired', 'Sales Professional', 'Scientist', 'Secretary/Front Office', 'Security Professional',
        'Self Employed', 'Service Engineering', 'Software Professional', 'Sports Person', 'Student', 'Teacher', 'Management (CXO, M.D. etc.)', 'Travel/Ticketing',
        'Web/Graphic Design', 'Others'

      ]
    }];
  };
  getFatherOccupation = function () {
    return [
      {
        name: 'Mostly Used',
        value: ['Business Person', 'Employed', 'Retired', 'Not Employed', 'Passed Away']
      },
      {
        name: 'Other Occupation',
        value: ['Looking for a job', 'Working', 'Actor/Model', 'Advertising Professional', 'Agent',
          'Agriculture/Dairy', 'Air Hostess', 'Architect', 'Banking Professional', 'Beautician',
          'BPO/ITES', 'Civil Services (IAS/ IFS/ IPS/ IRS)', 'Consultant',
          'Corporate Communication', 'Corporate Planning Professional', 'Customer Services', 'Defence', 'Doctor', 'Education Professional',
          'Engineer - Non IT', 'Export/Import', 'Fashion Designer', 'Financial Services/Accounting', 'Fitness Professional',
          'Govt. Services', 'Hardware/Telecom', 'Healthcare Professional', 'Hotels/Hospitality Professional', 'Professional', 'Interior Designer',
          'Journalist', 'Lawyer/Legal Professional', 'Logistics/SCM Professional', 'Marketing Professional', 'Media Professional', ' Merchant Navy',
          'NGO/Social Services', 'Nurse', 'Pilot', 'Police', 'Printing/Packaging', 'Professor/Lecturer', 'Project Manager - IT', 'Project Manager - Non IT',
          'Research Professional', 'Restaurateur', 'Sales Professional', 'Scientist', 'Secretary/Front Office', 'Security Professional',
          'Self Employed', 'Service Engineering', 'Software Professional', 'Sports Person', 'Student', 'Teacher', 'Management (CXO, M.D. etc.)', 'Travel/Ticketing',
          'Web/Graphic Design', 'Others']
      }

    ];
  };
  getMotherOccupation = function () {
    return [
      {
        name: 'Mostly Used',
        value: ['Homemaker', 'Employed', 'Retired', 'Not Employed', 'Passed Away']
      },
      {
        name: 'Other Occupation',
        value: ['Looking for a job', 'Working', 'Actor/Model', 'Advertising Professional', 'Agent',
          'Agriculture/Dairy', 'Air Hostess', 'Architect', 'Banking Professional', 'Beautician',
          'BPO/ITES', 'Civil Services (IAS/ IFS/ IPS/ IRS)', 'Consultant',
          'Corporate Communication', 'Corporate Planning Professional', 'Customer Services', 'Defence', 'Doctor', 'Education Professional',
          'Engineer - Non IT', 'Export/Import', 'Fashion Designer', 'Financial Services/Accounting', 'Fitness Professional',
          'Govt. Services', 'Hardware/Telecom', 'Healthcare Professional', 'Hotels/Hospitality Professional', 'Professional', 'Interior Designer',
          'Journalist', 'Lawyer/Legal Professional', 'Logistics/SCM Professional', 'Marketing Professional', 'Media Professional', ' Merchant Navy',
          'NGO/Social Services', 'Nurse', 'Pilot', 'Police', 'Printing/Packaging', 'Professor/Lecturer', 'Project Manager - IT', 'Project Manager - Non IT',
          'Research Professional', 'Restaurateur', 'Sales Professional', 'Scientist', 'Secretary/Front Office', 'Security Professional',
          'Self Employed', 'Service Engineering', 'Software Professional', 'Sports Person', 'Student', 'Teacher', 'Management (CXO, M.D. etc.)', 'Travel/Ticketing',
          'Web/Graphic Design', 'Others']
      }

    ];
  };
  getAccordanceData = function () {
    return [
      {
        heading: 'Home',
        iconClass: 'home',
        stateToGO: 'main',
        subHeadingEnable: false,
        subHeader: 'Menu',
        subHeaderEnabled: true,
        dividerEnabled: false
      },
      {
        heading: 'DashBoard',
        iconClass: 'dashboard',
        stateToGO: 'profile.home',
        subHeadingEnable: false
      },
      {
        heading: 'Profile',
        iconClass: 'person',
        stateToGO: 'profile.main',
        subHeadingEnable: true,
        subHeading: [
          {
            name: 'Personal Details',
            iconClass: 'mdi-account-plus',
            stateToGO: 'profile.personal'
          },
          {
            name: 'Education',
            iconClass: 'mdi-school',
            stateToGO: 'profile.education'
          },
          {
            name: 'Family',
            iconClass: 'mdi-account-multiple',
            stateToGO: 'profile.family'
          },
          {
            name: 'Profession',
            iconClass: 'mdi-laptop',
            stateToGO: 'profile.profession'
          },
          {
            name: 'About Me',
            iconClass: 'mdi-comment-account',
            stateToGO: 'profile.aboutme'
          },
          {
            name: 'You Likings',
            iconClass: 'mdi-glass-tulip',
            stateToGO: 'profile.lifestyle'
          },
          {
            name: 'Hobbies',
            iconClass: 'mdi-heart',
            stateToGO: 'profile.hobbies'
          },
          {
            name: 'Partner Preference',
            iconClass: 'mdi-account-switch',
            stateToGO: 'profile.partner'
          },
          {
            name: 'Photos',
            iconClass: 'mdi-image-album',
            stateToGO: 'profile.album'
          }
        ]
      },
      {
        heading: 'Alerts',
        iconClass: 'add_alert',
        stateToGO: 'profile.info',
        subHeadingEnable: false,
        subHeader: 'Pages',
        subHeaderEnabled: true,
        dividerEnabled: true
      },
      {
        heading: 'Messages',
        iconClass: 'message',
        stateToGO: 'profile.info',
        subHeadingEnable: false
      },
      {
        heading: 'Settings',
        iconClass: 'settings',
        stateToGO: 'setting.home',
        subHeadingEnable: true,
        subHeading: [
          {
            name: 'Edit',
            iconClass: 'mode_edit',
            stateToGO: 'setting.edit'
          },
          {
            name: 'Create',
            iconClass: 'add',
            stateToGO: 'setting.create'
          }
        ]
      }
    ];
  };
  getStates = function () {
    return {
      'Alabama': 'AL',
      'Alaska': 'AK',
      'American Samoa': 'AS',
      'Arizona': 'AZ',
      'Arkansas': 'AR',
      'California': 'CA',
      'Colorado': 'CO',
      'Connecticut': 'CT',
      'Delaware': 'DE',
      'District Of Columbia': 'DC',
      'Federated States Of Micronesia': 'FM',
      'Florida': 'FL',
      'Georgia': 'GA',
      'Guam': 'GU',
      'Hawaii': 'HI',
      'Idaho': 'ID',
      'Illinois': 'IL',
      'Indiana': 'IN',
      'Iowa': 'IA',
      'Kansas': 'KS',
      'Kentucky': 'KY',
      'Louisiana': 'LA',
      'Maine': 'ME',
      'Marshall Islands': 'MH',
      'Maryland': 'MD',
      'Massachusetts': 'MA',
      'Michigan': 'MI',
      'Minnesota': 'MN',
      'Mississippi': 'MS',
      'Missouri': 'MO',
      'Montana': 'MT',
      'Nebraska': 'NE',
      'Nevada': 'NV',
      'New Hampshire': 'NH',
      'New Jersey': 'NJ',
      'New Mexico': 'NM',
      'New York': 'NY',
      'North Carolina': 'NC',
      'North Dakota': 'ND',
      'Northern Mariana Islands': 'MP',
      'Ohio': 'OH',
      'Oklahoma': 'OK',
      'Oregon': 'OR',
      'Palau': 'PW',
      'Pennsylvania': 'PA',
      'Puerto Rico': 'PR',
      'Rhode Island': 'RI',
      'South Carolina': 'SC',
      'South Dakota': 'SD',
      'Tennessee': 'TN',
      'Texas': 'TX',
      'Utah': 'UT',
      'Vermont': 'VT',
      'Virgin Islands': 'VI',
      'Virginia': 'VA',
      'Washington': 'WA',
      'West Virginia': 'WV',
      'Wisconsin': 'WI',
      'Wyoming': 'WY'
    };
  };
  getGender = function () {
    return ['Male', 'Female'];
  };
  getMartialStatus = function () {
    return ['Single', 'Married', 'Divorce', 'Never Married'];
  };
  getBodyType = function () {
    return ['Slim', 'Average', 'Athletic', 'Heavy'];
  };
  getHiv = function () {
    return ['Yes', 'No'];
  };
  getThalassemia = function () {
    return ['Major', 'Minor', 'No'];
  };
  getProfileManagedBy = function () {
    return ['Self', 'Parents', 'Siblings', 'More'];
  };

  getHoroscopeMatch = function () {
    return ['Yes', 'No'];
  };

  getFamilyType = function () {
    return ['Joint', 'Nuclear', 'Others'];
  };

  getFamilyValues = function () {
    return ['Orthodox', 'Traditional', 'Moderate', 'Liberal'];
  };

  getFamilyStatus = function () {
    return ['Rich/Affluent', 'Upper Middle', 'Middle Class'];

  };
  getLivingWithParents = function () {
    return ['Yes', 'No', 'Not Applicable'];

  };

  getDietaryHabits = function () {
    return ['Vegetarian', 'Non Veg', 'Jain', 'Eggetarian'];

  };
  getDrinkingHabits = function () {
    return ['Yes', 'No', 'Occasionally'];

  };
  getSmokingHabits = function () {
    return ['Yes', 'No', 'Occasionally'];

  };
  getOpenTOPets = function () {
    return ['Yes', 'No'];

  };
  getOwnHouse = function () {
    return ['Yes', 'No'];

  };
  getOwnCar = function () {
    return ['Yes', 'No'];

  };

  getReligions = function () {
    return [{
      name: '',
      value: ['Hindu', 'Muslim', 'Christian', 'Sikh', 'Parsi', 'Jain', 'Buddhist', 'Jewish', 'No Religion', 'Spiritual', 'Other']
    }];
  };
  getPhysicallyChallenged = function () {
    return ['None', 'Physically By Birth', 'Physically By Accident', 'Mentally From Birth', 'Mentally By Accident'];
  };

  getResidentialStatus = function () {
    return ['Citizen', 'Permanent Resident', 'Work Permit', 'Student Visa', 'Temporary Visa'];
  };
  getBloodType = function () {
    return ['A+', 'A-', 'B+', 'B-', 'AB+', 'AB-', 'O+', 'O-'];
  };


  getHobbies() {
    return [
      {
        name: 'Hobbies',
        value: ['Collecting Stamps', 'Collecting Coins', 'Collecting antiques', 'Model building',
          'Gardening / Landscaping', 'Fishing', 'Bird watching', 'Taking care of pets', 'Playing musical instruments',
          'Singing', 'Dancing', 'Dancing', 'Ham radio', 'Astrology / Palmistry / Numerology', 'Graphology', 'Solving Crosswords, Puzzles',
          'Art / Handicraft', 'Painting', 'Cooking', 'Photography', 'Film-making']
      }
    ];
  }

  getInterests() {
    return [
      {
        name: 'Interests',
        value: ['Writing', 'Reading / Book clubs', 'Learning new languages', 'Listening to music', 'Movies', 'Theatre',
          'Watching television', 'Travel / Sightseeing', 'Sports - Outdoor', 'Sports - Indoor', 'Trekking / Adventure sports',
          'Video / Computer games', 'Health & Fitness', 'Yoga / Meditation', 'Alternative healing', 'Volunteering / Social Service',
          'Politics', 'Net surfing', 'Blogging']
      }
    ];
  }

  getFavouriteMusic() {
    return [
      {
        name: 'Favourite Music',
        value: ['Classical - Hindustani', 'Classical - Carnatic', 'Classical - Western', 'Instrumental - Indian', 'Instrumental - Western',
          'Old film songs', 'Latest film songs', 'Ghazals', 'Qawalis', 'Bhajans / Devotional', 'Sufi music', 'Indipop', 'Pop', 'Disco',
          'House Music', 'Techno', 'Hip-Hop', 'Rap', 'Jazz', 'Blues', 'Reggae', 'Heavy Metal', 'Acid Rock']
      }
    ];
  }

  getFavouriteBooks() {
    return [
      {
        name: 'Favourite Books',
        value: ['Classic literature', 'Biographies', 'History', 'Poetry', 'Romance', 'Thriller / Suspense', 'Humour', 'Science Fiction',
          'Fantasy', 'Business / Occupational', 'Philosophy / Spiritual', 'Self-help', 'Short stories', 'Comics', 'Magazines & Newspapers']
      }
    ];
  }

  getDressStyle() {
    return [
      {
        name: 'Dress Style',
        value: ['Classic Indian', 'Trendy', 'Classic Western', 'Designer', 'Casual']
      }

    ];
  }

  getFavouriteMovies() {
    return [
      {
        name: 'Favourite Movies',
        value: ['Classics', 'Romantic', 'Comedy', 'Action / Suspense', 'Sci-Fi & Fantasy', 'Horror', 'Non-commercial / Art', 'World cinema',
          'Documentaries', 'Short films', 'Epics', 'Drama']
      }
    ];
  }

  getSports() {
    return [
      {
        name: 'Sports',
        value: ['Jogging / Walking', 'Cycling', 'Swimming / Water sports', 'Cricket', 'Yoga / Meditation', 'Martial Arts', 'Hockey', 'Football',
          'Volleyball', 'Bowling', 'Chess', 'Carrom', 'Scrabble', 'Card games', 'Billiards / Snooker / Pool', 'Aerobics', 'Weight training',
          'Golf', 'Basketball', 'Tennis', 'Squash', 'Table-tennis', 'Badminton', 'Baseball', 'Rugby', 'Adventure sports']
      }

    ];
  }

  getCuisine() {
    return [
      {
        name: 'Cuisine',
        value: ['South Indian', 'Punjabi', 'Gujarati', 'Rajasthani', 'Bengali', 'Konkan', 'Chinese', 'Continental', 'Moghlai', 'Italian', 'Arabic',
          'Thai', 'Sushi', 'Mexican', 'Lebanese', 'Latin American', 'Spanish', 'Fast food']
      }
    ];
  }


  getDropDownList(dropDownName) {
    let resultList = [];
    if (dropDownName === 'gender') {
      resultList = this.getGender();
    } else if (dropDownName === 'higherEducation') {
      resultList = this.getHighestEducation();
    } else if (dropDownName === 'language' || dropDownName === 'Languages_I_Speak') {
      resultList = this.getLanguages();
      console.log('language', resultList);
    } else if (dropDownName === 'occupation') {
      resultList = this.getOccupation();
    } else if (dropDownName === 'countryLivingIn') {
      resultList = this.getCountries();
    } else if (dropDownName === 'annualIncome') {
      resultList = this.getAnnualIncome();
    } else if (dropDownName === 'complexion') {
      resultList = this.getComplexion();
    } else if (dropDownName === 'weight') {
      resultList = this.getWeightInKgs();
    } else if (dropDownName === 'height') {
      resultList = this.getHeight();
    } else if (dropDownName === 'martialStatus') {
      resultList = this.getMartialStatus();
    } else if (dropDownName === 'bodyType') {
      resultList = this.getBodyType();
    } else if (dropDownName === 'hiv') {
      resultList = this.getHiv();
    } else if (dropDownName === 'thalassemia') {
      resultList = this.getThalassemia();
    } else if (dropDownName === 'profileManagedBy') {
      resultList = this.getProfileManagedBy();
    } else if (dropDownName === 'religion') {
      resultList = this.getReligions();
    } else if (dropDownName === 'caste') {
      resultList = this.getCaste();
    } else if (dropDownName === 'sect') {
      resultList = this.getSect();
    } else if (dropDownName === 'sunSign') {
      resultList = this.getSunSign();
    } else if (dropDownName === 'nakshatra') {
      resultList = this.getNakshatra();
    } else if (dropDownName === 'moonSign') {
      resultList = this.getMoonSign();
    } else if (dropDownName === 'horoscopeMatch') {
      resultList = this.getHoroscopeMatch();
    } else if (dropDownName === 'fatherOccupation' || dropDownName === 'father') {
      resultList = this.getFatherOccupation();
    } else if (dropDownName === 'motherOccupation' || dropDownName === 'mother') {
      resultList = this.getMotherOccupation();
    } else if (dropDownName === 'familyValues') {
      resultList = this.getFamilyValues();
    } else if (dropDownName === 'familyType') {
      resultList = this.getFamilyType();
    } else if (dropDownName === 'familyStatus') {
      resultList = this.getFamilyStatus();
    } else if (dropDownName === 'livingWithParents') {
      resultList = this.getLivingWithParents();
    } else if (dropDownName === 'occupation') {
      resultList = this.getOccupation();
    } else if (dropDownName === 'relationship') {
      resultList = this.getRelationship();
    } else if (dropDownName === 'annualIncome') {
      resultList = this.getAnnualIncome();
    } else if (dropDownName === 'dietaryHabits') {
      resultList = this.getDietaryHabits();
    } else if (dropDownName === 'drinkingHabits') {
      resultList = this.getDrinkingHabits();
    } else if (dropDownName === 'smokingHabits') {
      resultList = this.getSmokingHabits();
    } else if (dropDownName === 'openToPets') {
      resultList = this.getOpenTOPets();
    } else if (dropDownName === 'ownHouse') {
      resultList = this.getOwnHouse();
    } else if (dropDownName === 'ownCar') {
      resultList = this.getOwnCar();
    } else if (dropDownName === 'residentialStatus') {
      resultList = this.getResidentialStatus();
    } else if (dropDownName === 'bloodType') {
      resultList = this.getBloodType();
    } else if (dropDownName === 'age') {
      resultList = this.getAge();
    } else if (dropDownName === 'hobbies') {
      resultList = this.getHobbies();
    } else if (dropDownName === 'interests') {
      resultList = this.getInterests();
    } else if (dropDownName === 'favouriteMusic') {
      resultList = this.getFavouriteMusic();
    } else if (dropDownName === 'favouriteBook') {
      resultList = this.getFavouriteBooks();
    } else if (dropDownName === 'dressStyle') {
      resultList = this.getDressStyle();
    } else if (dropDownName === 'favouriteMovies') {
      resultList = this.getFavouriteMovies();
    } else if (dropDownName === 'sports') {
      resultList = this.getSports();
    } else if (dropDownName === 'cuisine') {
      resultList = this.getCuisine();
    }
    // console.log('list', resultList);
    return resultList;
  }
}

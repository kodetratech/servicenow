import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {AuthenticationService} from './authentication.service';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class BackEndService {

    private baseUrl = environment.hostName + environment.appUrl;

    constructor(public http: HttpClient,
                private authenticationService: AuthenticationService) {
    }

    getHeader() {
        return new HttpHeaders().set('Authorization', 'Bearer ' + this.authenticationService.getToken());
    }

    getOrders(pageNumber, limit): Observable<any> {
        return this.http.get(this.baseUrl + '/order', {
            headers: this.getHeader(),
            params: new HttpParams().set('page', pageNumber).set('limit', limit),
            observe: 'response'
        })
            .map(this.extractData).catch(this.handleError);
    }

    updateOrderStatus(status): Observable<any> {
        return this.http.put(this.baseUrl + '/order/update/status', status, {
            headers: this.getHeader(),
            observe: 'response'
        })
            .map(this.extractData).catch(this.handleError);
    }

    private extractData(res: HttpResponse<any>) {
        // let headers = new Headers(res.headers);
        const response = {
            data: res.body.data,
            total: res.headers.get('X-Total-Docs'),
            limit: res.headers.get('X-Limit'),
            page: res.headers.get('X-Page'),
            pages: res.headers.get('X-Pages'),
        };
        return response;
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        const errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(error);
    }
}

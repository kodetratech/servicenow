import {Injectable} from '@angular/core';
import {ValidationService} from './validation.service';

@Injectable({
  providedIn: 'root'
})
export class FormValidationService {

  constructor(public validationService: ValidationService) {
  }

  onFocus(formElement) {
    formElement.fieldClass = 'is-focused';
    formElement.fieldLabel = 'show-label';
    if (formElement.fieldType === 'date') {
      formElement.elementType = 'date';
    }
    return formElement;
  }

  onBlur(formElement, formGroup) {
    const theField = formGroup.controls[formElement.elementName];
    if (theField.errors !== null && theField.hasError('required')) {
      formElement.fieldClass = 'is-invalid';
      formElement.fieldLabel = 'hide-label';
    } else if (theField.invalid) {
      formElement.fieldClass = 'is-invalid';
    } else {
      if (theField.value.length === 0) {
        formElement.fieldLabel = 'hide-label';
        formElement.fieldClass = 'is-empty';
      } else {
        formElement.fieldClass = 'is-valid';
      }
    }
    return formElement;
  }

  getErrorMessages(formElement, formGroup) {
    for (const propertyName in formGroup.controls[formElement.elementName].errors) {
      if (formGroup.controls[formElement.elementName].errors.hasOwnProperty(propertyName) && formGroup.controls[formElement.elementName].touched) {
        return this.validationService.getValidatorErrorMessage(propertyName, formGroup.controls[formElement.elementName].errors[propertyName]);
      }
    }
    return null;
  }
}

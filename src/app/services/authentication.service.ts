import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private baseUrl = environment.hostName + environment.appUrl;

  constructor(public http: HttpClient) {
  }

  getToken() {
    const token = localStorage.getItem('token');
    if (token && token !== null) {
      return token;
    } else {
      return null;
    }
  }

  getUserId() {
    const userId = localStorage.getItem('userId');
    if (userId && userId !== null) {
      return userId;
    } else {
      return null;
    }
  }

  getUser() {
    const user = localStorage.getItem('user');
    if (user && user !== null) {
      return JSON.parse(user);
    } else {
      return null;
    }
  }

  getUserRole() {
    const user = JSON.parse(localStorage.getItem('user'));
    if (user && user !== null) {
      return user.role;
    } else {
      return null;
    }
  }

  isAuthenticated() {
    const token = this.getToken();
    const userId = this.getUserId();
    return !!((token && token !== null) && (userId && userId !== null));
  }


  loginByFacebook() {
    location.href = environment.hostName + '/auth/facebook';
  }

  loginByGoogle() {
    location.href = environment.hostName + '/auth/google';
  }

  loginByTwitter() {
    location.href = environment.hostName + '/auth/twitter';
  }

  login(request): Observable<any> {
    return this.http.post(this.baseUrl + '/user/login', request)
      .map((response: any) => {
        // login successful if there's a jwt token in the response
        const loginResponse = response.data;
        const token = loginResponse.token;
        if (token) {
          // set token property
          // store username and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('userId', loginResponse.user._id);
          localStorage.setItem('user', JSON.stringify(loginResponse.user));
          localStorage.setItem('token', loginResponse.token);
          // return true to indicate successful login
          return loginResponse;
        } else {
          // return false to indicate failed login
          return false;
        }
      }).map((loginResponse: any) => {
        return loginResponse;
      });
  }

  registration(userData): Observable<any> {
    return this.http.post(this.baseUrl + '/user/create', {user: userData})
      .map((response: any) => {
        // login successful if there's a jwt token in the response
        return response;
      }).catch(this.handleError);
  }

  forgetPassword(request): Observable<any> {
    return this.http.post(this.baseUrl + '/user/forget/password', request)
      .map((response: any) => {
        // login successful if there's a jwt token in the response
        return response;
      }).catch(this.handleError);
  }

  resetPassword(request): Observable<any> {
    return this.http.post(this.baseUrl + '/user/reset/password', request)
      .map((response: any) => {
        // login successful if there's a jwt token in the response
        return response;
      }).catch(this.handleError);
  }

  logout() {
    localStorage.clear();
  }

  private extractData(res: HttpResponse<any>) {
    // let headers = new Headers(res.headers);
    // console.log("headers data loaded " , headers);
    return {
      data: res.body.data,
      total: res.headers.get('X-Total-Docs'),
      limit: res.headers.get('X-Limit'),
      page: res.headers.get('X-Page'),
      pages: res.headers.get('X-Pages'),
    };
  }

  private handleError(error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    const errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(error);
  }
}

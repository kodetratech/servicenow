import {EventEmitter, Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/share';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
  spinnerStatus: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  start() {
    this.spinnerStatus.emit({status: true});
  }

  stop() {
    this.spinnerStatus.emit({status: false});
  }

  getSpinnerEmitter() {
    return this.spinnerStatus;
  }

}

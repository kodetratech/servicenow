import {Injectable} from '@angular/core';
import {ToastController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class ToastMessageService {
    constructor(public toastCtrl: ToastController) {
    }

    async presentToast(message) {
        const toast = await this.toastCtrl.create({
            message: message,
            duration: 5000,
            position: 'top',  // 'top','bottom','middle'
            showCloseButton: false,
            closeButtonText: 'Close',
        });
        toast.present();
    }
}

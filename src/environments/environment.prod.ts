export const environment = {
    production: true,
    hostName: 'https://localhost:8444',
    appUrl: '/api/v1/kuviknotes'
};

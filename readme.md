ionic cordova platform add android
ionic cordova platform rm android
ionic cordova platform add ios

# Sign Android APK

# prod build android 
ionic cordova build android --aot --uglifyjs --minifyjs --minifycss --release
or 
ionic cordova build --release --prod android
ionic cordova run android --device
ionic cordova run browser --prod --release

# Build for android platform 
  ## for android production build 
  ERROR: CALL_AND_RETRY_LAST Allocation failed - JavaScript heap out of memory
  use this command : 
  export NODE_OPTIONS=--max-old-space-size=4096
  ionic cordova build  --prod --release android --device
  
    ionic cordova run  --prod  android --device
   
  1. ionic cordova build --prod --release android
  2. cd platforms/android/app/build/outputs/apk/release/
  3. app-release-unsigned.apk should be there, if your build is successful

## Follow the instructions in the Android Studio Help Center to generate a new key. It must be different from any previous keys. Alternatively, you can use the following command line to generate a new key:
keytool -genkeypair -alias upload -keyalg RSA -keysize 2048 -validity 9125 -keystore keystore.jks

## This key must be a 2048 bit RSA key and have 25-year validity. Export the certificate for that key to PEM format:
keytool -export -rfc -alias upload -file upload_certificate.pem -keystore keystore.jks

# Generate private key
  keytool -genkey -v -keystore my-release-key.jks -keyalg RSA -keysize 2048 -validity 9125 -alias my-alias
  
  #provided by google customer care
  keytool -genkeypair -alias upload -keyalg RSA -keysize 2048 -validity 9125 -keystore keystore.jks
  
# Sign your unsigned APK file
  jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.jks app-release-unsigned.apk my-alias

# Go to Android Studio build tool folder
  cd ~/Library/Android/sdk/build-tools/29.0.0
cd ~/Library/Android/sdk/build-tools/29.0.0
#  Run the zip align tool to optimize the APK
   ~/Library/Android/sdk/build-tools/29.0.0/zipalign -v 4 app-release-unsigned.apk convenix.apk

# Verify that your apk is signed run apksigner
  ~/Library/Android/sdk/build-tools/29.0.0/apksigner verify convenix.apk

# To get list of outdated packages and plugins
npm outdated

# get the plugin list
cordova plugin list


# production build
npm run ionic:build -- --prod


# Xcode 10 uses a new build system by default, command to build app for ios platform
ionic cordova build ios -- --buildFlag="-UseModernBuildSystem=0"

ionic cordova run ios -- --buildFlag="-UseModernBuildSystem=0" --device

##Create a Podfile if you don't have one:
pod init

##Open your Podfile and add:
pod 'Firebase/Core'

##Includes Analytics by default and Save the file and run:
pod install


#The Google services plugin for Gradle loads the google-services.json file that you just downloaded. Modify your build.gradle files to use the plugin.

#Project-level build.gradle (<project>/build.gradle):

buildscript {
  dependencies {
    // Add this line
    classpath 'com.google.gms:google-services:4.0.1'
  }
}

#App-level build.gradle (<project>/<app-module>/build.gradle):

dependencies {
  // Add this line
  implementation 'com.google.firebase:firebase-core:16.0.1'
}
#Add to the bottom of the file
apply plugin: 'com.google.gms.google-services'

## For web application
<script src="https://www.gstatic.com/firebasejs/5.6.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyDaD5Cja4tBFsaD5xveK98wFkl-GdK1yIU",
    authDomain: "convenix-44469.firebaseapp.com",
    databaseURL: "https://convenix-44469.firebaseio.com",
    projectId: "convenix-44469",
    storageBucket: "convenix-44469.appspot.com",
    messagingSenderId: "924101545247"
  };
  firebase.initializeApp(config);
</script>

# Android device debugging

https://medium.com/@coderonfleek/debugging-an-ionic-android-app-using-chrome-dev-tools-6e139b79e8d2


ionic cordova plugin add uk.co.workingedge.phonegap.plugin.launchnavigator --variable GOOGLE_API_KEY_FOR_ANDROID="AIzaSyCsi_T5aHlhD_aqsUfoVAXRbCyHeap4qNY"

#upload files to s3 bucket
aws s3 sync ./www s3://convenix.com --profile kodetra

# typscript
npm install typescript@">=3.4.0 <3.5.0" --save


# Error
  ld: library not found for -lGoogleToolboxForMac
  Solution : go to platforms/ios and run 'pod install'

  ERROR: CALL_AND_RETRY_LAST Allocation failed - JavaScript heap out of memory
  use this command : 
  export NODE_OPTIONS=--max-old-space-size=4096
  ionic cordova build  --prod --release android --device
